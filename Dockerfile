FROM php:8.0-apache
# install base module
RUN apt update \
    && apt install -y libzip-dev zip libpng-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
  && docker-php-ext-configure gd --with-freetype --with-jpeg \
  && docker-php-ext-install -j$(nproc) gd \
  && docker-php-ext-install zip \
  && docker-php-ext-install mysqli \
  && docker-php-ext-install calendar \
  && a2enmod rewrite
# enable redis
RUN  pecl install -o -f redis \
  && rm -rf /tmp/pear \
  && docker-php-ext-enable redis
# install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY ./. /var/www/html
RUN composer install --working-dir=/var/www/html/assets/import/PHPMailer
RUN composer install --working-dir=/var/www/html/assets/import/php-jwt
# RUN composer install --working-dir=/var/www/html/assets/import/meilisearch
