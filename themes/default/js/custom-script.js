
async function updateTokenForInteractingVideo({ api_url, action, video_key, token, comment_id}){
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: api_url + '/v1/video/update-token-interacting-video',
            contentType: 'application/json',
            data: JSON.stringify({action, video_key, comment_id }),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + token);
            },
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        });
    });
}

function toHHMMSS(secs){
    var sec_num = parseInt(secs, 10)
    var hours   = Math.floor(sec_num / 3600)
    var minutes = Math.floor(sec_num / 60) % 60
    var seconds = sec_num % 60

    return [hours,minutes,seconds]
        .map(v => v < 10 ? "0" + v : v)
        .filter((v,i) => v !== "00" || i > 0)
        .join(":")
}

function isFileImage(file) {
    return file && file['type'].split('/')[0] === 'image';
}