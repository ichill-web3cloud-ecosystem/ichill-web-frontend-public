function onClickCommentLiked(object, eventType, commentId, api_url, type, videoKey, token ){
    PT_LikeComments(object, eventType, commentId);
    updateTokenForInteractingVideo({
        api_url,
        action: type,
        video_key: videoKey,
        token,
        comment_id : commentId
    }).then(r => null);
}

function onClickCommentReplies(value, commentId,  event, videoId, videoKey, api_url, token ){
    PT_RVReply(value, commentId, event, videoId, videoKey, api_url, token);
}

function onClickSubscribe(userId, isSubscribedButton, api_url, type, videoKey, token ){
    PT_Subscribe(userId, isSubscribedButton)
    updateTokenForInteractingVideo({
        api_url,
        action: type,
        video_key: videoKey,
        token
    }).then(r => null);
}

function onClickLikeDislikeVideo(object, type, videoId, videoKey, api_url, token){
    Wo_LikeSystem(videoId, type, object, 'is_ajax')
    updateTokenForInteractingVideo({
        api_url,
        action: type,
        video_key: videoKey,
        token
    }).then(r => null);
}

function getCommentsVideo(url, video_id, pageComment){
    $.ajax({
        url,
        type: 'GET',
        dataType: 'json',
        data: {  video_id, page : pageComment },
        beforeSend: () => {
        }
    })
    .done(function(data) {
         if (data.status === 200) {
             $('#loading-spinner-comments').hide();
             $("#video-user-comments").append(data.html);
             $("#count_comments").text(data.count_comments);
             isLoadMoreComments = data.isLoadMoreComment
         }
    });
}


function make_skeleton_next_video() {
    var output = `<div class="video-wrapper top-video-wrapper pt_video_side_vids pt_pt_mn_wtch_rltvids" >
                      <div style="padding: unset; border: none; background-color: unset; ">
                          <div class="ph-col-4" style="padding-left: unset; margin-bottom: unset; padding-right: unset; " >
                              <div class="ph-picture" style="height: 184px;"></div>
                          </div>
                          <div>
                              <div class="ph-row">
                                  <div class="ph-col-12"></div>
                                  <div class="ph-col-2"></div>
                                  <div class="ph-col-10 empty"></div>
                                  <div class="ph-col-8 big"></div>
                                  <div class="ph-col-4 big empty"></div>
                              </div>
                          </div>
                      </div>
                  </div>`;
    return output;
}

function getVideoNextSideBar(url, video_id ){
    $.ajax({
        url,
        type: 'GET',
        dataType: 'json',
        data: {  video_id },
        beforeSend: () => {
            $("#next-video").html(make_skeleton_next_video());
        }
    })
        .done(function(data) {
            if (data.status === 200) {
                $("#next-video").html(data.html);
                $("#next-video-mobile").html(data.html);
            }
        });
}

function setSizeContainerInContainer(){
    if($('#my-video_youtube_iframe').length > 0 && heightContainerInContainer > 0 &&  widthContainerInContainer > 0){
        $('.mejs__container').css('height', heightContainerInContainer + 'px');
        $('.mejs__container').css('width', widthContainerInContainer + 'px');
    }
}

async function rewardInteractAds({action, api_url, ads_id, fingerprint, video_key, token }){
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: api_url + '/v1/advertising/reward-interact',
            contentType: 'application/json',
            data: JSON.stringify({action, ads_id, fingerprint, video_key }),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", 'Bearer ' + token);
            },
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        });
    });
}


async function addHistoryViewAds({ api_url, ad_id, video_key, token }) {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: 'POST',
            url: api_url + '/v1/user-ads/add-history-view',
            contentType: 'application/json',
            data: JSON.stringify({ ad_id , video_key }),
            beforeSend: function(xhr) {
                xhr.setRequestHeader ("Authorization",  'Bearer ' + token );
            },
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    })
}

// async function updateReachImpression({ api_url, ad_id, video_key, token, type_interact }) {
//     return new Promise((resolve, reject) => {
//         $.ajax({
//             type: 'POST',
//             url: api_url + '/v1/user-ads/update-reach-impression',
//             contentType: 'application/json',
//             data: JSON.stringify({ad_id, video_key, type_interact}),
//             beforeSend: function (xhr) {
//                 xhr.setRequestHeader("Authorization", 'Bearer ' + token);
//             },
//             success: function (data) {
//                 resolve(data)
//             },
//             error: function (error) {
//                 reject(error)
//             },
//         });
//     });
// }

// async function rewardAdvertising({fingerprint, api_url, video_ads_id, type_ads, user_key, video_key, token, action}) {
//     return new Promise((resolve, reject) => {
//         let url = `${api_url}/v1/advertising/reward-ads`;
//         $.ajax({
//             type: "POST",
//             url: url,
//             data: {
//                 action,
//                 video_ads_id,
//                 type_ads,
//                 fingerprint,
//                 user_key,
//                 video_key
//             },
//             dataType: 'json',
//             beforeSend: function (xhr) {
//                 xhr.setRequestHeader("Authorization", 'Bearer ' + token);
//             },
//             success: function (data, status) {
//                 resolve(data)
//             },
//             error: function (err) {
//                 reject(err)
//             },
//         });
//     });
// }

// async function updateClickAds({ api_url, ad_id, video_key, token  }) {
//     return new Promise((resolve, reject) => {
//         $.ajax({
//             type: 'POST',
//             url: api_url + '/v1/user-ads/update-click',
//             contentType: 'application/json',
//             data: JSON.stringify({ ad_id , video_key }),
//             beforeSend: function(xhr) {
//                 xhr.setRequestHeader ("Authorization",  'Bearer ' + token );
//             },
//             success: function (data, status) {
//                 resolve(data)
//             },
//             error: function (err) {
//                 reject(err)
//             },
//         });
//     });
// }


async function addTrackingAdvertising({ api_url, video_ads_id, type_ads,  fingerprint, user_key, video_key, duration }){
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "POST",
            url: `${api_url}/v1/tracking-ads/add`,
            data: {
                video_ads_id ,
                type_ads ,
                fingerprint ,
                user_key ,
                video_key ,
                duration
            },
            dataType: 'json',
            success: function (data, status) {
                resolve(data)
            },
            error: function (err) {
                reject(err)
            },
        });
    });

}