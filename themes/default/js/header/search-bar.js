
const inputSearch = document.getElementById('search-bar');
var focusIndex = -1;
var prevFocusIndex = focusIndex;
var isChangeValueSearch = false;

inputSearch.onfocus = function () {
    this.classList.add('search-selected');
    setColorItemSearch()

    focusIndex = -1;
    prevFocusIndex = focusIndex;
}
inputSearch.onblur = function () {
    this.classList.remove('search-selected');
}

function onChangeInputSearch(e) {
    isChangeValueSearch = true;
    focusIndex = -1;
    prevFocusIndex = focusIndex;

    const search_value = $('#search-bar').val();
    searchListVideo(search_value)
}

function setColorItemSearch(){
    $(`.search-header .search-item-${prevFocusIndex}`).css({ 'background-color' : 'unset' })
    if($("body").hasClass( "night-mode" )){
        $(`.search-header .search-item-${focusIndex}`).css({ 'background-color' : 'gray' })
    }else if($("body").hasClass( "light-mode" )){
        $(`.search-header .search-item-${focusIndex}`).css({ 'background-color' : 'lightgray' })
    }
}

document.body.addEventListener('keydown', (e) => {
    if (e.target.classList.contains('search-selected')  ) { //&& $('#search-results').children().length > 0
        const  totalItemSearch = document.getElementById("search-results").children.length;
        if (e.keyCode === 38 && focusIndex > 0) {  // key up
            isChangeValueSearch = false;
            focusIndex--;
            prevFocusIndex = focusIndex + 1;
            setColorItemSearch()
        }
        if(e.keyCode === 40 && focusIndex === totalItemSearch - 1 ){
            focusIndex = -1;
            prevFocusIndex = totalItemSearch - 1;
        }
        if (e.keyCode === 40 && focusIndex < totalItemSearch - 1) { //key down
            isChangeValueSearch = false;
            if(focusIndex === -1){
                $(`.search-header .search-item-${prevFocusIndex}`).css({ 'background-color' : 'unset' })
            }
            focusIndex++;
            prevFocusIndex = focusIndex - 1;
            setColorItemSearch()
        }

        if(e.keyCode === 13){ // key enter
            if(!isChangeValueSearch && $('#search-results').children().length > 0){
                const href = $(`.search-header .search-item-${focusIndex} a`).attr('href')
                window.location.href = href;
            }else{
                let searchValue = $('#search-bar').val();
                window.location.href = linkSearch + '?keyword=' + searchValue;
            }
        }
    }
});

const inputSearchMobile = document.getElementById('search-bar-mobile');
inputSearchMobile.addEventListener("keypress", function(event) {
    if (event.key === "Enter") {
        event.preventDefault();
        let searchValue = $('#search-bar-mobile').val();
        window.location.href = linkSearch + '?keyword=' + searchValue;
    }
});