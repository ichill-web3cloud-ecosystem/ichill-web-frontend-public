
function make_skeleton_trending_list(limitPageSize = 5) {
    let output = '';
    for (let count = 0; count < limitPageSize; count++) {
        output += `<div class="col-md-3 col-sm-6 col-xs-6 keep-padding"">
                          <div class="video-list short">
                              <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
                                <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                                  <div class="ph-picture" ></div>
                                  <div class="ph-row">
                                    <div class="ph-col-10 big"></div>
                                    <div class="ph-col-6"></div>
                                    <div class="ph-col-6 empty"></div>
                                    <div class="ph-col-7"></div>
                                    <div class="ph-col-5 empty"></div>
                                    <div class="ph-col-8 big"></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                   </div>`;
    }
    return output;
}

function make_skeleton_trending_title(){
    const html = `<h4>
            <div class="ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
            <div class="view_more_link ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
          </h4>`;
    return html;
}

function trending_title(title, explore_more){
    const  html = `<h4><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#2196F3" d="M16.85 6.85l1.44 1.44-4.88 4.88-3.29-3.29c-.39-.39-1.02-.39-1.41 0l-6 6.01c-.39.39-.39 1.02 0 1.41.39.39 1.02.39 1.41 0L9.41 12l3.29 3.29c.39.39 1.02.39 1.41 0l5.59-5.58 1.44 1.44c.31.31.85.09.85-.35V6.5c.01-.28-.21-.5-.49-.5h-4.29c-.45 0-.67.54-.36.85z"></path></svg> ${title}
            <span class="view_more_link"><a href="{{LINK videos/trending}}" data-load="?link1=videos&page=trending"> ${explore_more} </a></span>
          </h4>`;
    return html;
}

function showTrendingList(url, title, explore_more){
        $.ajax({
            url,
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {
                $("#trending-title").html(make_skeleton_trending_title());

                let skeleton_trending = make_skeleton_trending_list()
                $("#trending-list-lazy-loading").html(skeleton_trending);
            },
            success: function(data) {
                if (data.status === 200) {
                    $("#trending-title").html(trending_title(title, explore_more));

                    $( "#trending-list-lazy-loading" ).remove();
                    if(data.html === ""){
                        $('#trending-list-to').css({"display": "none"})
                        // $('#trending-list').html(`<h5 class="empty_state"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" class="feather"><path fill="currentColor" d="M21 14.2V8.91c0-.89-1.08-1.34-1.71-.71L17 10.5V7c0-.55-.45-1-1-1h-5.61l8.91 8.91c.62.63 1.7.18 1.7-.71zM2.71 2.56c-.39.39-.39 1.02 0 1.41L4.73 6H4c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.21 0 .39-.08.55-.18l2.48 2.48c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L4.12 2.56c-.39-.39-1.02-.39-1.41 0z"/></svg> ${data.no_videos_found_for_now} </h5>`);
                    }else{
                        $("#trending-list").html(data.html);
                    }
                }
            }
        });
}
