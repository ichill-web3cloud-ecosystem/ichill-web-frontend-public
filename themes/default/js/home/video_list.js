

function getVideosCategory(url){
    // let hT = $('#home-page-videos').offset().top,
    //     hH = $('#home-page-videos').outerHeight(),
    //     wH = $(window).height(),
    //     wS = $(this).scrollTop();
    // if ((wS > (hT + hH - wH)) && (hT > wS) && (wS + wH > hT + hH) && !showVideoCategory
    // ){
    //     showVideoCategory = true;
        $.ajax({
            url,
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {
                $("#home-page-videos").html(`<div style="display: flex; justify-content: center"> <div class="loader"></div> </div>`);
            },
            success: function(data) {
                if (data.status === 200) {
                    $("#home-page-videos").html(data.html);
                }
            }
        });
    // };

}