


function make_skeleton_top_list(limitPageSize = 5) {
    let output = '';
    for (let count = 0; count < limitPageSize; count++) {
        output += `<div class="col-md-3 col-sm-6 col-xs-6 keep-padding"">
                          <div class="video-list short">
                              <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
                                <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                                  <div class="ph-picture" ></div>
                                  <div class="ph-row">
                                    <div class="ph-col-10 big"></div>
                                    <div class="ph-col-6"></div>
                                    <div class="ph-col-6 empty"></div>
                                    <div class="ph-col-7"></div>
                                    <div class="ph-col-5 empty"></div>
                                    <div class="ph-col-8 big"></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                   </div>`;
    }
    return output;
}

function make_skeleton_top_title(){
    const html = `<h4>
            <div class="ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
            <div class="view_more_link ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
          </h4>`;
    return html;
}


function top_title(title, explore_more){
    const  html = `<h4><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#359b39" d="M6.4 9.2h.2c.77 0 1.4.63 1.4 1.4v7c0 .77-.63 1.4-1.4 1.4h-.2c-.77 0-1.4-.63-1.4-1.4v-7c0-.77.63-1.4 1.4-1.4zM12 5c.77 0 1.4.63 1.4 1.4v11.2c0 .77-.63 1.4-1.4 1.4-.77 0-1.4-.63-1.4-1.4V6.4c0-.77.63-1.4 1.4-1.4zm5.6 8c.77 0 1.4.63 1.4 1.4v3.2c0 .77-.63 1.4-1.4 1.4-.77 0-1.4-.63-1.4-1.4v-3.2c0-.77.63-1.4 1.4-1.4z"></path></svg> ${title}
            <span class="view_more_link"><a href="{{LINK videos/top}}" data-load="?link1=videos&page=top"> ${explore_more} </a></span>
          </h4>`;
    return html;
}


function getTopList(url, title, explore_more){
    // let hT = $('#top-list-to').offset().top,
    //     hH = $('#top-list-to').outerHeight(),
    //     wH = $(window).height(),
    //     wS = $(this).scrollTop();
    // if (wS > (hT + hH - wH) && (hT > wS) && (wS + wH > hT + hH) && !showTop
    // ){
    //     showTop = true;
        $.ajax({
            url ,
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {
                $("#top-title").html(make_skeleton_top_title());

                let skeleton_top = make_skeleton_top_list()
                $("#top-list-lazy-loading").html(skeleton_top);
            },
            success: function(data) {
                if (data.status === 200) {
                    $("#top-title").html(top_title(title, explore_more));

                    $( "#top-list-lazy-loading" ).remove();
                    if(data.html === ""){
                        $('#top-list').html(`<h5 class="empty_state"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" class="feather"><path fill="currentColor" d="M21 14.2V8.91c0-.89-1.08-1.34-1.71-.71L17 10.5V7c0-.55-.45-1-1-1h-5.61l8.91 8.91c.62.63 1.7.18 1.7-.71zM2.71 2.56c-.39.39-.39 1.02 0 1.41L4.73 6H4c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.21 0 .39-.08.55-.18l2.48 2.48c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L4.12 2.56c-.39-.39-1.02-.39-1.41 0z"/></svg> ${data.no_videos_found_for_now}</h5>`);
                    }else{
                        $("#top-list").html(data.html);
                    }
                }
            }
        });
    // }

}