

function make_skeleton_live_list(limitPageSize = 5) {
    let output = '';
    for (let count = 0; count < limitPageSize; count++) {
        output += `<div class="col-md-3 col-sm-6 col-xs-6 keep-padding"">
                          <div class="video-list short">
                              <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
                                <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                                  <div class="ph-picture" ></div>
                                  <div class="ph-row">
                                    <div class="ph-col-10 big"></div>
                                    <div class="ph-col-6"></div>
                                    <div class="ph-col-6 empty"></div>
                                    <div class="ph-col-7"></div>
                                    <div class="ph-col-5 empty"></div>
                                    <div class="ph-col-8 big"></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                   </div>`;
    }
    return output;
}

function make_skeleton_live_title(){
    const html = `<h4>
            <div class="ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
            <div class="view_more_link ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
          </h4>`;
    return html;
}

function live_title(title, explore_more){
    const  html = `<h4><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f44336" d="M17,10.5L21,6.5V17.5L17,13.5V17A1,1 0 0,1 16,18H4A1,1 0 0,1 3,17V7A1,1 0 0,1 4,6H16A1,1 0 0,1 17,7V10.5M14,16V15C14,13.67 11.33,13 10,13C8.67,13 6,13.67 6,15V16H14M10,8A2,2 0 0,0 8,10A2,2 0 0,0 10,12A2,2 0 0,0 12,10A2,2 0 0,0 10,8Z"></path></svg> ${title}
            <span class="view_more_link"><a href="{{LINK videos/live}}" data-load="?link1=videos&page=live"> ${explore_more} </a></span>
          </h4>`
    return html;
}


function getLiveList(url, title, explore_more){
    // let hT = $('#live-list-to').offset().top,
    //     hH = $('#live-list-to').outerHeight(),
    //     wH = $(window).height(),
    //     wS = $(this).scrollTop();
    // if (wS > (hT + hH - wH) && (hT > wS) && (wS + wH > hT + hH) && !showLive
    // ){
    //     showLive = true;
        $.ajax({
            url ,
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {
                $("#live-title").html(make_skeleton_live_title());

                let skeleton_top = make_skeleton_live_list()
                $("#live-list-lazy-loading").html(skeleton_top);
            },
            success: function(data) {
                if (data.status === 200) {
                    $("#live-title").html(live_title(title, explore_more));

                    $("#live-list-lazy-loading").remove();
                    if(data.html === ""){
                        $("#live-list-to").css({"display": "none"})
                        // $('#live-list').html(`<h5 class="empty_state"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" class="feather"><path fill="currentColor" d="M21 14.2V8.91c0-.89-1.08-1.34-1.71-.71L17 10.5V7c0-.55-.45-1-1-1h-5.61l8.91 8.91c.62.63 1.7.18 1.7-.71zM2.71 2.56c-.39.39-.39 1.02 0 1.41L4.73 6H4c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.21 0 .39-.08.55-.18l2.48 2.48c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L4.12 2.56c-.39-.39-1.02-.39-1.41 0z"/></svg> ${data.no_videos_found_for_now}</h5>`);
                    }else{
                        $("#live-list").html(data.html);
                    }
                }
            }
        });
    // }

}