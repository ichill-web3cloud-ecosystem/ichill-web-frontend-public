

function initSwiperShorts(key, rtl = false){
    $(key).slick({
            infinite: false,
            slidesToShow: 6,
            rtl,
            slidesToScroll: 2,
            responsive: [
                {
                    breakpoint: 1360,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 1251,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 1127,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 910,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 705,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 530,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                        waitForAnimate: false,
                    }
                },
                {
                    breakpoint: 485,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        waitForAnimate: false,
                    }
                }
            ]
    });
}

function make_skeleton_short_list(limitPageSize = 6) {
    let output = '';
    for (let count = 0; count < limitPageSize; count++) {
        output += `<div class="col-md-3 col-sm-6 col-xs-6 keep-padding"">
                          <div class="video-list short">
                              <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
                                <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                                  <div class="ph-picture" style="padding-bottom: 100%;"></div>
                                  <div class="ph-row">
                                    <div class="ph-col-10 big"></div>
                                    <div class="ph-col-6"></div>
                                    <div class="ph-col-6 empty"></div>
                                    <div class="ph-col-7"></div>
                                    <div class="ph-col-5 empty"></div>
                                    <div class="ph-col-8 big"></div>
                                  </div>
                                </div>
                              </div>
                          </div>
                   </div>`;
    }
    return output;
}

function short_title(title, explore_more){
    const  html = `<div class="title" id="short-title">
          <h4>
            <svg id="Layer_1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64"><defs><style>.cls-1{fill:url(#linear-gradient);}.cls-2{fill:#fff;}.cls-3{fill:url(#linear-gradient-2);}</style><linearGradient id="linear-gradient" x1="58.85" y1="32" x2="-0.74" y2="32" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#51c8ec"/><stop offset="1" stop-color="#3a88c8"/></linearGradient><linearGradient id="linear-gradient-2" x1="37.13" y1="29.09" x2="2.64" y2="32.31" xlink:href="#linear-gradient"/></defs><path class="cls-1" d="M21.77,5.5,56.72,27a7.52,7.52,0,0,1,3.06,2.55,4.64,4.64,0,0,1-.14,5,12.3,12.3,0,0,1-3.6,2.9L21.74,58.52a5.22,5.22,0,0,1-8-4.52V9.89C13.7,5.69,18,3.32,21.77,5.5Z"/><path class="cls-2" d="M27.18,14.52,48.85,27.86c2.22,1.55,2.48,6.06-.09,7.83L27.18,49.2c-2.3,1.61-4.69.06-4.69-3v-29C22.49,14.11,24.88,12.91,27.18,14.52Z"/><path class="cls-3" d="M38.22,27.77,10.55,10.58a4.61,4.61,0,0,0-7,4V49a4.61,4.61,0,0,0,7,4L38.22,35.83A4.78,4.78,0,0,0,38.22,27.77Z"/><path class="cls-2" d="M13.66,38.13V51.07l8.83-5.48C22.69,38.55,13.66,38.13,13.66,38.13Z"/><circle class="cls-2" cx="18.08" cy="31.93" r="5.29"/></svg>
               ${title}
            <span class="view_more_link"><a href="{{LINK shorts}}" data-load="?link1=shorts&id="> ${explore_more} </a></span>
          </h4>
        </div>`;
    return html;
}


function make_skeleton_short_title(){
    const html = `<h4>
            <div class="ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
            <div class="view_more_link ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
          </h4>`;
    return html;
}