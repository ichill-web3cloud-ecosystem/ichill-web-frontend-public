
function updateVideoOnLatest(){
    document.querySelector(".pt_latest_vds").querySelectorAll(".video-list-token-info")[0].style.display = "none"
    document.querySelector(".pt_latest_vds").querySelectorAll(".video-list-title")[0].style.bottom = "30px"
    document.querySelector(".pt_latest_vds").style.display = "unset"

    const h4TitleOnLatest = document.querySelector(".pt_latest_vds").querySelectorAll("h4")
    for (const element of h4TitleOnLatest) {
        element.style.cssText = "height: unset !important;"
    }
    const titleVideoOnLastest = document.querySelector(".pt_latest_vds").querySelectorAll(".video-list-by")[0]
    titleVideoOnLastest.style.cssText = "display: unset;"
    titleVideoOnLastest.querySelectorAll('span')[0].style.cssText = "display: unset;"
    titleVideoOnLastest.querySelectorAll('a')[0].style.cssText = "display: unset;"
}

function make_skeleton_latest_list() {
    let output = `
        <div class="row">
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
              <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                <div class="ph-picture"></div>
                <div class="ph-row">
                  <div class="ph-col-10 big"></div>
                  <div class="ph-col-6"></div>
                  <div class="ph-col-6 empty"></div>
                  <div class="ph-col-7"></div>
                  <div class="ph-col-5 empty"></div>
                  <div class="ph-col-8 big"></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-sm-12 col-xs-12">
            <div class="row">
              <div class="col-md-6 col-sm-12 col-xs-12 keep-padding">
                <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
                  <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                    <div class="ph-picture"></div>
                    <div class="ph-row">
                      <div class="ph-col-10 big"></div>
                      <div class="ph-col-6"></div>
                      <div class="ph-col-6 empty"></div>
                      <div class="ph-col-7"></div>
                      <div class="ph-col-5 empty"></div>
                      <div class="ph-col-8 big"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12 keep-padding">
                <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
                  <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                    <div class="ph-picture"></div>
                    <div class="ph-row">
                      <div class="ph-col-10 big"></div>
                      <div class="ph-col-6"></div>
                      <div class="ph-col-6 empty"></div>
                      <div class="ph-col-7"></div>
                      <div class="ph-col-5 empty"></div>
                      <div class="ph-col-8 big"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-12 col-xs-12 keep-padding">
                <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
                  <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                    <div class="ph-picture"></div>
                    <div class="ph-row">
                      <div class="ph-col-10 big"></div>
                      <div class="ph-col-6"></div>
                      <div class="ph-col-6 empty"></div>
                      <div class="ph-col-7"></div>
                      <div class="ph-col-5 empty"></div>
                      <div class="ph-col-8 big"></div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12 col-xs-12 keep-padding">
                <div class="ph-item" style="margin-bottom: unset; padding: unset; border: unset; background-color: unset; ">
                  <div class="ph-col-12" style="margin-bottom: unset; padding-left: unset; padding-right: unset;">
                    <div class="ph-picture"></div>
                    <div class="ph-row">
                      <div class="ph-col-10 big"></div>
                      <div class="ph-col-6"></div>
                      <div class="ph-col-6 empty"></div>
                      <div class="ph-col-7"></div>
                      <div class="ph-col-5 empty"></div>
                      <div class="ph-col-8 big"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>`;

    return output;
}

function make_skeleton_latest_title(){
    const html = `<h4>
            <div class="ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
            <div class="view_more_link ph-item" style="margin-bottom: unset; padding-top: unset; border: unset; background-color: #ced4da">
              <div class="ph-col-10 big"></div>
            </div>
          </h4>`;
    return html;
}

function latest_title(title, explore_more){
    const  html = `<h4><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#e91e63" d="M3 6c-.55 0-1 .45-1 1v13c0 1.1.9 2 2 2h13c.55 0 1-.45 1-1s-.45-1-1-1H5c-.55 0-1-.45-1-1V7c0-.55-.45-1-1-1zm17-4H8c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h12c1.1 0 2-.9 2-2V4c0-1.1-.9-2-2-2zm-8 12.5v-9l5.47 4.1c.27.2.27.6 0 .8L12 14.5z"></path></svg> ${title}
            <span class="view_more_link"><a href="{{LINK videos/latest}}" data-load="?link1=videos&page=latest"> ${explore_more} </a></span>
          </h4>`;
    return html;
}

function getLatestVideo(url, title, explore_more){
    // let hT = $('#latest-list-to').offset().top,
    //     hH = $('#latest-list-to').outerHeight(),
    //     wH = $(window).height(),
    //     wS = $(this).scrollTop();
    // if (wS > (hT + hH - wH) && (hT > wS) && (wS + wH > hT + hH) && !showLatest
    // ){
    //     showLatest = true;
        $.ajax({
            url,
            type: 'GET',
            dataType: 'json',
            beforeSend: function() {
                $("#latest-title").html(make_skeleton_latest_title());

                let skeleton_latest = make_skeleton_latest_list()
                $("#latest-list-lazy-loading").html(skeleton_latest);
            },
            success: function(data) {
                if (data.status === 200) {
                    $("#latest-title").html(latest_title(title, explore_more));

                    $( "#latest-list-lazy-loading" ).remove();
                    if(data.html === ""){
                        $("#latest-list-to").css({"display": "none"})
                        // $('#latest-list').html(`<h5 class="empty_state"><svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24" class="feather"><path fill="currentColor" d="M21 14.2V8.91c0-.89-1.08-1.34-1.71-.71L17 10.5V7c0-.55-.45-1-1-1h-5.61l8.91 8.91c.62.63 1.7.18 1.7-.71zM2.71 2.56c-.39.39-.39 1.02 0 1.41L4.73 6H4c-.55 0-1 .45-1 1v10c0 .55.45 1 1 1h12c.21 0 .39-.08.55-.18l2.48 2.48c.39.39 1.02.39 1.41 0 .39-.39.39-1.02 0-1.41L4.12 2.56c-.39-.39-1.02-.39-1.41 0z"/></svg> ${data.no_videos_found_for_now}</h5>`);
                    }else{
                        $("#latest-list").html(data.html);
                    }
                    updateVideoOnLatest()
                }
            }
        });
    // }
}