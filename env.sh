export backend_port=8001
export mysql_port=8002
export web_port=8003

export redis_backend_port=8004
export redis_web_port=8005

export web_hotname=${env}.web
export backend_hostname=${env}.backend
export mysql_hostname=${env}.mysql
export redis_web_hostname=${env}.web.redis
export redis_backend_hostname=${env}.backend.redis

export network=${env}_ichill

export web_dir=web
export volumes_dir=/data/volumes
export config_dir=/data/config
export backup_dir=/data/backup

# web_source_dir=.
export web_upload_dir=${volumes_dir}/${env}/${web_dir}/upload
export web_config_php=${config_dir}/${env}/${web_dir}/config.php
export web_config_json=${config_dir}/${env}/${web_dir}/config.json
export mysql_dir=db
export mysql_env=${config_dir}/${env}/${mysql_dir}/mysql.env
export mysql_db_dir=${volumes_dir}/${env}/${mysql_dir}/mysql