<?php
// +------------------------------------------------------------------------+
// | @author Deen Doughouz (DoughouzForest)
// | @author_url 1: http://www.playtubescript.com
// | @author_url 2: http://codecanyon.net/user/doughouzforest
// | @author_email: wowondersocial@gmail.com
// +------------------------------------------------------------------------+
// | PlayTube - The Ultimate Video Sharing Platform
// | Copyright (c) 2017 PlayTube. All rights reserved.
// +------------------------------------------------------------------------+



function removeIndexSearchVideo($video_key): void
{
    global $api_url;
    $url = $api_url . '/v1/video/remove-index-search';
    $data_string = json_encode(array(
        'video_key' => $video_key,
    ));
    $jwt_token = get_jwt_token();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt(
        $ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer '.$jwt_token]
    );
    $request = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if (curl_errno($ch)) {
        $error_msg = curl_error($ch);
        echo $error_msg;
        http_response_code($httpcode);
        exit();
    }

}

function indexSearchVideo($video_key): void
{
    global $api_url;
    $url = $api_url . '/v1/video/index-search';
    $data_string = json_encode(array(
        'video_key' => $video_key,
    ));
    $jwt_token = get_jwt_token();
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt(
        $ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'Authorization: Bearer '.$jwt_token]
    );
    $request = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    if (curl_errno($ch)) {
        $error_msg = curl_error($ch);
        echo $error_msg;
        http_response_code($httpcode);
        exit();
    }

}

function getStatusAds($status_ad = ''): string
{
    global $StatusAds, $lang;
    return match ($status_ad) {
        $StatusAds->Waiting => $lang->status_waiting_ads,
        $StatusAds->On => $lang->status_on_ads,
        $StatusAds->Off => $lang->status_off_ads,
        $StatusAds->Rejected => $lang->status_rejected_ads,
        $StatusAds->Deleted => $lang->status_deleted_ads,
        default => '',
    };
}

function PT_ShareFileV1($data = array(), $type = 0): array|bool
{
    global $pt, $mysqli;
    $allowed = '';
    if (!file_exists('upload/photos/' . date('Y'))) {
        @mkdir('upload/photos/' . date('Y'), 0777, true);
    }
    if (!file_exists('upload/photos/' . date('Y') . '/' . date('m'))) {
        @mkdir('upload/photos/' . date('Y') . '/' . date('m'), 0777, true);
    }
    if (!file_exists('upload/timeline/' . date('Y') . '/' . date('m'))) {
        @mkdir('upload/timeline/' . date('Y') . '/' . date('m'), 0777, true);
    }
    if (!file_exists('upload/videos/' . date('Y'))) {
        @mkdir('upload/videos/' . date('Y'), 0777, true);
    }
    if (!file_exists('upload/videos/' . date('Y') . '/' . date('m'))) {
        @mkdir('upload/videos/' . date('Y') . '/' . date('m'), 0777, true);
    }
    if (!file_exists('upload/photos/' . date('Y').'/index.html')) {
        @file_put_contents('upload/photos/' . date('Y').'/index.html','index.html');
    }
    if (!file_exists('upload/photos/' . date('Y') . '/' . date('m').'/index.html')) {
        @file_put_contents('upload/photos/' . date('Y') . '/' . date('m').'/index.html','index.html');
    }
    if (!file_exists('upload/videos/' . date('Y').'/index.html')) {
        @file_put_contents('upload/videos/' . date('Y').'/index.html','index.html');
    }
//    if (!file_exists('upload/videos/' . date('Y') . '/' . date('m').'/index.html')) {
//        @file_put_contents('upload/videos/' . date('Y') . '/' . date('m').'/index.html','index.html');
//    }
    if (isset($data['file']) && !empty($data['file'])) {
        $data['file'] = $data['file'];
    }
    if (isset($data['name']) && !empty($data['name'])) {
        $data['name'] = PT_Secure($data['name']);
    }
    if (isset($data['name']) && !empty($data['name'])) {
        $data['name'] = PT_Secure($data['name']);
    }
    if (empty($data)) {
        return false;
    }
    $allowed           = 'jpg,png,jpeg,gif,mp4,mov,webm,mpeg,3gp,mkv,mk3d,mks';
    if (!empty($data['allowed'])) {
        $allowed  = $data['allowed'];
    }
    $new_string        = pathinfo($data['name'], PATHINFO_FILENAME) . '.' . strtolower(pathinfo($data['name'], PATHINFO_EXTENSION));
    $extension_allowed = explode(',', $allowed);
    $file_extension    = pathinfo($new_string, PATHINFO_EXTENSION);
    if (!in_array($file_extension, $extension_allowed)) {
        return array(
            'error' => 'File format not supported'
        );
    }
    if ($file_extension == 'jpg' || $file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'gif') {
        $folder   = 'photos';
        $fileType = 'image';
    } else {
        $folder   = 'videos';
        $fileType = 'video';
    }
    if (empty($folder) || empty($fileType)) {
        return false;
    }
    $ar = array(
        'video/mp4',
        'video/mov',
        'video/3gp',
        'video/3gpp',
        'video/mpeg',
        'video/flv',
        'video/avi',
        'video/webm',
        'audio/wav',
        'audio/mpeg',
        'video/quicktime',
        'audio/mp3',
        'image/png',
        'image/jpeg',
        'image/gif',
        'video/x-msvideo',
        'video/msvideo',
        'video/x-ms-wmv',
        'video/x-flv',
        'video/x-matroska',
        'video/webm'
    );

    if (!in_array($data['type'], $ar)) {
        return array(
            'error' => 'File format not supported'
        );
    }
    $dir         = "upload/{$folder}/" . date('Y') . '/' . date('m');
    $filename    = $dir . '/' . PT_GenerateKey() . '_' . date('d') . '_' . md5(time()) . "_{$fileType}.{$file_extension}";
    $second_file = pathinfo($filename, PATHINFO_EXTENSION);

//    $pre_signed_url =  PT_GETPreSignedPostURLS3($filename );

//    if (move_uploaded_file($data['file'], $filename)) {
        if ($second_file == 'jpg' || $second_file == 'jpeg' || $second_file == 'png' || $second_file == 'gif') {
            if ($type == 1) {
                @PT_CompressImage($filename, $filename, 50);
                $explode2  = @end(explode('.', $filename));
                $explode3  = @explode('.', $filename);
                $last_file = $explode3[0] . '_small.' . $explode2;
                @PT_Resize_Crop_Image(400, 400, $filename, $last_file, 60);

                if (($pt->config->s3_upload == 'on' || $pt->config->ftp_upload == 'on' || $pt->config->spaces == 'on' || $pt->config->wasabi_storage == 'on') && !empty($last_file)) {
                    $upload_s3 = PT_UploadToS3($last_file);
                }
            }

            else {
                if ($second_file != 'gif') {
                    if ($type == 2) {
                        $explode2  = @end(explode('.', $filename));
                        $explode3  = @explode('.', $filename);
                        $last_file = $explode3[0] . '_full.' . $explode2;
                        @PT_CompressImage($filename, $last_file, 100);
                    }

                    if (!empty($data['crop'])) {
                        $crop_image = PT_Resize_Crop_Image($data['crop']['width'], $data['crop']['height'], $filename, $filename, 80);
                    }
                    @PT_CompressImage($filename, $filename, 90);
                }

//                if (($pt->config->s3_upload == 'on' || $pt->config->ftp_upload == 'on' || $pt->config->spaces == 'on' || $pt->config->wasabi_storage == 'on') && !empty($filename)) {
//                    $upload_s3 = PT_UploadToS3($filename);
//                }
            }
        }

        else{
            if (($pt->config->s3_upload == 'on' || $pt->config->ftp_upload == 'on' || $pt->config->spaces == 'on' || $pt->config->wasabi_storage == 'on') && !empty($filename)) {
                $upload_s3 = PT_UploadToS3($filename);
            }
        }

        $last_data             = array();
        $last_data['filename'] = $filename;
        $last_data['name']     = $data['name'];
//        $last_data['pre_signed_url'] = $pre_signed_url;
        return $last_data;
//    }
}


function PT_UpdatePointsAndBalance($type_action = '', $video_id = 0, $action_like_dislike = '' ): void
{
    global $db, $pt, $Creator, $user, $TypeActionVideo;
    if(!empty($type_action) && $video_id > 0 ){
        $point = 0;
        $get_video = PT_GetVideoByID($video_id, 0, 1, 2);
        switch ($type_action) {
            case $TypeActionVideo->Comment:
                $point = $Creator->VIDEO_COMMENTED;
                break;
            case $TypeActionVideo->Like:
            {
                if($action_like_dislike == 'deleted_like' ){
                    $point = - $Creator->VIDEO_LIKED;
                }else if($action_like_dislike == 'added_like'  ){
                    $point = $Creator->VIDEO_LIKED;
                }
                break;
            }
            case $TypeActionVideo->View:
                $point = $Creator->VIDEO_VIEWED;
                break;
            default:
                break;
        }

        $points_video = $point + $get_video->points;
        $ud_video = array('points' =>  $points_video );
        $db->where('id', $video_id)->update(T_VIDEOS, $ud_video);

        $points_user = $point + $get_video->owner->points;
        $balance_user = $get_video->owner->balance + $point;
        $ud_user = array('points' => $points_user, 'balance' => $balance_user );
        $db->where('id', $get_video->user_id)->update(T_USERS, $ud_user);

    }
}

function PT_TokenCalculationForOneComment($user_id = 0, $video_id = 0, $comment_id = 0): float|int
{ // $user_id of the user commented $video_id
    global $db, $pt;
    $get_video = PT_GetVideoByID($video_id, 0, 1, 2);
    $count_comments  = $db->where('video_id', $get_video->id)->where('user_id',$pt->blocked_array , 'NOT IN')->getValue(T_COMMENTS, 'count(*)');
    $video_comment =  $db->where('id', $comment_id)->getOne(T_COMMENTS);

    $total_liked = $get_video->likes;
    $total_viewed = $get_video->views;
    $total_commented = $count_comments;

    $num_viewed = 0;
    $num_liked = 0;
    $num_commented = 0;
    $num_liked_on_comment = $video_comment->likes;
    $num_video = 1;
    $is_trend = PT_IsVideoTrending($get_video->video_id);
    $percentage = ($num_liked + $num_liked_on_comment + $num_commented + $num_viewed) /  ($total_liked + $total_viewed + $total_commented); # TODO: Add num voted of comment
    return PT_get_viewer_token($num_liked, $num_commented, $num_liked_on_comment, $num_video, $is_trend, $percentage );

}

function PT_IsVideoTrending($video_key = ''): bool
{
    global $pt, $db;
    $is_trend = false;
    $videos = $db->where('video_id', $video_key)->where('privacy', 0)->where('user_id',$pt->blocked_array , 'NOT IN')->where('time', time() - 172800, '>')->where('is_movie',0)->where('live_time',0)->where('approved',1)->getOne(T_VIDEOS);
    if($videos->id > 0){
        $is_trend = true;
    }
    return $is_trend;
}

function PT_TokenCalculationForUser($user_id = 0): float|int
{
    global $pt, $db;
    $videos = $db->where('user_id', $user_id)->where('user_id',$pt->blocked_array , 'NOT IN')->where('is_movie',0)->orderBy('id', 'DESC')->get(T_VIDEOS);
    $token_user = 0;
    foreach($videos as $key => $video){
        $token_video =  PT_TokenCalculationForVideo($video->video_id);
        $token_user += $token_video;
    }
    return $token_user;
}


function PT_TokenCalculationForVideo($video_key = ''): float|int
{
    global $pt, $db, $Viewer, $Total;
    $get_video = PT_GetVideoByID($video_key, 1, 1);

    $total_commented  = $db->where('video_id', $get_video->id)->where('user_id',$pt->blocked_array , 'NOT IN')->getValue(T_COMMENTS, 'count(*)');
    // $total_subcribed = $db->where('user_id', $get_video->user_id)->getValue(T_SUBSCRIPTIONS, "count(*)");

    $is_trend = PT_IsVideoTrending($video_key);

    $total_viewed = $get_video -> views;
    $total_liked = $get_video->likes;
    $total_disliked = $get_video->dislikes;
    $num_video = 1;
    return PT_get_creator_token($total_viewed, $total_liked, $total_commented, $total_disliked, $num_video, $is_trend );
}


function PT_get_creator_token($total_viewed = 0, $total_liked = 0, $total_commented = 0, $total_disliked = 0,
                              $num_video = 1, $is_trend = false  ): float|int
{
    global $Creator ;
    $viewed_token = $total_viewed * $Creator->VIDEO_VIEWED;
    $liked_token = $total_liked * $Creator->VIDEO_LIKED;
    $commented_token = $total_commented * $Creator->$VIDEO_COMMENTED;
    $disliked_token = $total_disliked * $Creator->VIDEO_DISLIKED;
    $trending_token = 0;
    if($is_trend){
        $trending_token = $Creator->VIDEO_TRENDING;
    }

    return ($viewed_token + $liked_token + $commented_token + $disliked_token + $trending_token) * $num_video;
}


function PT_get_viewer_token($num_liked = 0, $num_commented = 0, $num_liked_on_comment = 0,  $num_video = 1,
                             $is_trend = false, $percentage = 0 ): float|int
{
    global $Viewer ;
    $liked = $num_liked * $Viewer->LIKED;
    $commented_token = $num_commented * $Viewer->COMMENTED;
    $comment_liked = $num_liked_on_comment * $Viewer->COMMENT_LIKED;
    $trending_token = 0;
    if($is_trend){
        # When 1 video trending, thousands of viewers contribute to it
        $trending_token = $Viewer->VIDEO_TRENDING * $percentage;
    }

    return $num_video * ($liked + $commented_token + $comment_liked + $trending_token);
}

function CryptoJSAesEncrypt($passphrase, $plain_text): bool|string
{

    $salt = openssl_random_pseudo_bytes(256);
    $iv = openssl_random_pseudo_bytes(16);
    //on PHP7 can use random_bytes() istead openssl_random_pseudo_bytes()
    //or PHP5x see : https://github.com/paragonie/random_compat

    $iterations = 999;
    $key = hash_pbkdf2("sha512", $passphrase, $salt, $iterations, 64);

    $encrypted_data = openssl_encrypt($plain_text, 'aes-256-cbc', hex2bin($key), OPENSSL_RAW_DATA, $iv);

    $data = array("ciphertext" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "salt" => bin2hex($salt));
    return json_encode($data);
}

function EventTrackingGA4($postdata = array()): void
{
    global  $pt;
    $measurement_id = $pt->config->google;
    $api_secret = $pt->config->api_secret_ga4 ;
    $url = 'https://www.google-analytics.com/mp/collect?&measurement_id=' . $measurement_id . '&api_secret=' . $api_secret;
    $headers = [
        'Content-Type: application/json',
    ];

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postdata) );  //Post Fields
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $request = curl_exec($ch);
    $result = json_decode($request);

    curl_close ($ch);
}

function PT_LoadPage($page_url = '', $data = array(), $set_lang = true) {
    global $pt, $lang_array, $config, $fl_currpage, $countries_name,$db;
    $page = './themes/' . $config['theme'] . '/layout/' . $page_url . '.html';
    if (!file_exists($page)) {
        die("File not Exists : $page");
    }
    $page_content = '';
    ob_start();
    require($page);
    $page_content = ob_get_contents();
    ob_end_clean();
    if ($set_lang == true) {
        $page_content = preg_replace_callback("/{{LANG (.*?)}}/", function($m) use ($lang_array) {
            return (isset($lang_array[$m[1]])) ? $lang_array[$m[1]] : '';
        }, $page_content);
    }
    if (!empty($data) && is_array($data)) {
        foreach ($data as $key => $replace) {
            if ($key == 'USER_DATA') {
                $replace = ToArray($replace);
                $page_content = preg_replace_callback("/{{USER (.*?)}}/", function($m) use ($replace) {
                    return (isset($replace[$m[1]])) ? $replace[$m[1]] : '';
                }, $page_content);
            } else {
                $object_to_replace = "{{" . $key . "}}";
                $page_content      = str_replace($object_to_replace, $replace, $page_content);
            }
        }
    }
    if (IS_LOGGED == true) {
        $replace = ToArray($pt->user);
        $page_content = preg_replace_callback("/{{ME (.*?)}}/", function($m) use ($replace) {
            return (isset($replace[$m[1]])) ? $replace[$m[1]] : '';
        }, $page_content);
    }
    $page_content = preg_replace("/{{LINK (.*?)}}/", PT_Link("$1"), $page_content);
    $page_content = preg_replace_callback("/{{CONFIG (.*?)}}/", function($m) use ($config) {
        return (isset($config[$m[1]])) ? $config[$m[1]] : '';
    }, $page_content);
    return $page_content;
}
function PT_LoadAdminPage($page_url = '', $data = array(), $set_lang = true) {
    global $pt, $lang_array, $config, $db;
    $page = './admin-panel/pages/' . $page_url . '.html';
    if (!file_exists($page)) {
        return false;
    }
    $page_content = '';
    ob_start();
    require($page);
    $page_content = ob_get_contents();
    ob_end_clean();
    if ($set_lang == true) {
        $page_content = preg_replace_callback("/{{LANG (.*?)}}/", function($m) use ($lang_array) {
            return (isset($lang_array[$m[1]])) ? $lang_array[$m[1]] : '';
        }, $page_content);
    }
    if (!empty($data) && is_array($data)) {
        foreach ($data as $key => $replace) {
            if ($key == 'USER_DATA') {
                $replace = ToArray($replace);
                $page_content = preg_replace_callback("/{{USER (.*?)}}/", function($m) use ($replace) {
                    return (isset($replace[$m[1]])) ? $replace[$m[1]] : '';
                }, $page_content);
            } else {
                $object_to_replace = "{{" . $key . "}}";
                $page_content      = str_replace($object_to_replace, $replace, $page_content);
            }
        }
    }
    if (IS_LOGGED == true) {
        $replace = ToArray($pt->user);
        $page_content = preg_replace_callback("/{{ME (.*?)}}/", function($m) use ($replace) {
            return (isset($replace[$m[1]])) ? $replace[$m[1]] : '';
        }, $page_content);
    }
    $page_content = preg_replace("/{{LINK (.*?)}}/", PT_Link("$1"), $page_content);
    $page_content = preg_replace_callback("/{{CONFIG (.*?)}}/", function($m) use ($config) {
        return (isset($config[$m[1]])) ? $config[$m[1]] : '';
    }, $page_content);
    return $page_content;
}
function PT_Link($string) {
    global $site_url;
    return $site_url . '/' . $string;
}
function PT_Slug($string, $video_id) {
    global $pt;
    if ($pt->config->seo_link != 'on') {
        return $video_id;
    }
    $slug = url_slug($string, array(
        'delimiter' => '-',
        'limit' => 100,
        'lowercase' => true,
        'replacements' => array(
            '/\b(an)\b/i' => 'a',
            '/\b(example)\b/i' => 'Test'
        )
    ));
    return $slug . '_' . $video_id . '.html';
}
function PT_URLSlug($string, $id) {
    global $pt;
    $slug = url_slug($string, array(
        'delimiter' => '-',
        'limit' => 100,
        'lowercase' => true,
        'replacements' => array(
            '/\b(an)\b/i' => 'a',
            '/\b(example)\b/i' => 'Test'
        )
    ));
    return $slug . '_' . $id . '.html';
}
function PT_LoadAdminLinkSettings($link = '') {
    global $site_url;
    return $site_url . '/admin-cp/' . $link;
}
function PT_LoadAdminLink($link = '') {
    global $site_url;
    return $site_url . '/admin-panel/' . $link;
}
function url_slug($str, $options = array()) {
    // Make sure string is in UTF-8 and strip invalid UTF-8 characters
    $str      = mb_convert_encoding((string) $str, 'UTF-8', mb_list_encodings());
    $defaults = array(
        'delimiter' => '-',
        'limit' => null,
        'lowercase' => true,
        'replacements' => array(),
        'transliterate' => false
    );
    // Merge options
    $options  = array_merge($defaults, $options);
    $char_map = array(
        // Latin
        'À' => 'A',
        'Á' => 'A',
        'Â' => 'A',
        'Ã' => 'A',
        'Ä' => 'A',
        'Å' => 'A',
        'Æ' => 'AE',
        'Ç' => 'C',
        'È' => 'E',
        'É' => 'E',
        'Ê' => 'E',
        'Ë' => 'E',
        'Ì' => 'I',
        'Í' => 'I',
        'Î' => 'I',
        'Ï' => 'I',
        'Ð' => 'D',
        'Ñ' => 'N',
        'Ò' => 'O',
        'Ó' => 'O',
        'Ô' => 'O',
        'Õ' => 'O',
        'Ö' => 'O',
        'Ő' => 'O',
        'Ø' => 'O',
        'Ù' => 'U',
        'Ú' => 'U',
        'Û' => 'U',
        'Ü' => 'U',
        'Ű' => 'U',
        'Ý' => 'Y',
        'Þ' => 'TH',
        'ß' => 'ss',
        'à' => 'a',
        'á' => 'a',
        'â' => 'a',
        'ã' => 'a',
        'ä' => 'a',
        'å' => 'a',
        'æ' => 'ae',
        'ç' => 'c',
        'è' => 'e',
        'é' => 'e',
        'ê' => 'e',
        'ë' => 'e',
        'ì' => 'i',
        'í' => 'i',
        'î' => 'i',
        'ï' => 'i',
        'ð' => 'd',
        'ñ' => 'n',
        'ò' => 'o',
        'ó' => 'o',
        'ô' => 'o',
        'õ' => 'o',
        'ö' => 'o',
        'ő' => 'o',
        'ø' => 'o',
        'ù' => 'u',
        'ú' => 'u',
        'û' => 'u',
        'ü' => 'u',
        'ű' => 'u',
        'ý' => 'y',
        'þ' => 'th',
        'ÿ' => 'y',
        // Latin symbols
        '©' => '(c)',
        // Greek
        'Α' => 'A',
        'Β' => 'B',
        'Γ' => 'G',
        'Δ' => 'D',
        'Ε' => 'E',
        'Ζ' => 'Z',
        'Η' => 'H',
        'Θ' => '8',
        'Ι' => 'I',
        'Κ' => 'K',
        'Λ' => 'L',
        'Μ' => 'M',
        'Ν' => 'N',
        'Ξ' => '3',
        'Ο' => 'O',
        'Π' => 'P',
        'Ρ' => 'R',
        'Σ' => 'S',
        'Τ' => 'T',
        'Υ' => 'Y',
        'Φ' => 'F',
        'Χ' => 'X',
        'Ψ' => 'PS',
        'Ω' => 'W',
        'Ά' => 'A',
        'Έ' => 'E',
        'Ί' => 'I',
        'Ό' => 'O',
        'Ύ' => 'Y',
        'Ή' => 'H',
        'Ώ' => 'W',
        'Ϊ' => 'I',
        'Ϋ' => 'Y',
        'α' => 'a',
        'β' => 'b',
        'γ' => 'g',
        'δ' => 'd',
        'ε' => 'e',
        'ζ' => 'z',
        'η' => 'h',
        'θ' => '8',
        'ι' => 'i',
        'κ' => 'k',
        'λ' => 'l',
        'μ' => 'm',
        'ν' => 'n',
        'ξ' => '3',
        'ο' => 'o',
        'π' => 'p',
        'ρ' => 'r',
        'σ' => 's',
        'τ' => 't',
        'υ' => 'y',
        'φ' => 'f',
        'χ' => 'x',
        'ψ' => 'ps',
        'ω' => 'w',
        'ά' => 'a',
        'έ' => 'e',
        'ί' => 'i',
        'ό' => 'o',
        'ύ' => 'y',
        'ή' => 'h',
        'ώ' => 'w',
        'ς' => 's',
        'ϊ' => 'i',
        'ΰ' => 'y',
        'ϋ' => 'y',
        'ΐ' => 'i',
        // Turkish
        'Ş' => 'S',
        'İ' => 'I',
        'Ç' => 'C',
        'Ü' => 'U',
        'Ö' => 'O',
        'Ğ' => 'G',
        'ş' => 's',
        'ı' => 'i',
        'ç' => 'c',
        'ü' => 'u',
        'ö' => 'o',
        'ğ' => 'g',
        // Russian
        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ё' => 'Yo',
        'Ж' => 'Zh',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'J',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'H',
        'Ц' => 'C',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Sh',
        'Ъ' => '',
        'Ы' => 'Y',
        'Ь' => '',
        'Э' => 'E',
        'Ю' => 'Yu',
        'Я' => 'Ya',
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'yo',
        'ж' => 'zh',
        'з' => 'z',
        'и' => 'i',
        'й' => 'j',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'h',
        'ц' => 'c',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'sh',
        'ъ' => '',
        'ы' => 'y',
        'ь' => '',
        'э' => 'e',
        'ю' => 'yu',
        'я' => 'ya',
        // Ukrainian
        'Є' => 'Ye',
        'І' => 'I',
        'Ї' => 'Yi',
        'Ґ' => 'G',
        'є' => 'ye',
        'і' => 'i',
        'ї' => 'yi',
        'ґ' => 'g',
        // Czech
        'Č' => 'C',
        'Ď' => 'D',
        'Ě' => 'E',
        'Ň' => 'N',
        'Ř' => 'R',
        'Š' => 'S',
        'Ť' => 'T',
        'Ů' => 'U',
        'Ž' => 'Z',
        'č' => 'c',
        'ď' => 'd',
        'ě' => 'e',
        'ň' => 'n',
        'ř' => 'r',
        'š' => 's',
        'ť' => 't',
        'ů' => 'u',
        'ž' => 'z',
        // Polish
        'Ą' => 'A',
        'Ć' => 'C',
        'Ę' => 'e',
        'Ł' => 'L',
        'Ń' => 'N',
        'Ó' => 'o',
        'Ś' => 'S',
        'Ź' => 'Z',
        'Ż' => 'Z',
        'ą' => 'a',
        'ć' => 'c',
        'ę' => 'e',
        'ł' => 'l',
        'ń' => 'n',
        'ó' => 'o',
        'ś' => 's',
        'ź' => 'z',
        'ż' => 'z',
        // Latvian
        'Ā' => 'A',
        'Č' => 'C',
        'Ē' => 'E',
        'Ģ' => 'G',
        'Ī' => 'i',
        'Ķ' => 'k',
        'Ļ' => 'L',
        'Ņ' => 'N',
        'Š' => 'S',
        'Ū' => 'u',
        'Ž' => 'Z',
        'ā' => 'a',
        'č' => 'c',
        'ē' => 'e',
        'ģ' => 'g',
        'ī' => 'i',
        'ķ' => 'k',
        'ļ' => 'l',
        'ņ' => 'n',
        'š' => 's',
        'ū' => 'u',
        'ž' => 'z'
    );
    // Make custom replacements
    $str      = preg_replace(array_keys($options['replacements']), $options['replacements'], $str);
    // Transliterate characters to ASCII
    if ($options['transliterate']) {
        $str = str_replace(array_keys($char_map), $char_map, $str);
    }
    // Replace non-alphanumeric characters with our delimiter
    $str = preg_replace('/[^\p{L}\p{Nd}]+/u', $options['delimiter'], $str);
    // Remove duplicate delimiters
    $str = preg_replace('/(' . preg_quote($options['delimiter'], '/') . '){2,}/', '$1', $str);
    // Truncate slug to max. characters
    $str = mb_substr($str, 0, ($options['limit'] ? $options['limit'] : mb_strlen($str, 'UTF-8')), 'UTF-8');
    // Remove delimiter from ends
    $str = trim($str, $options['delimiter']);
    return $options['lowercase'] ? mb_strtolower($str, 'UTF-8') : $str;
}
function br2nl($st) {
    $breaks = array(
        "<br />",
        "<br>",
        "<br/>"
    );
    return str_ireplace($breaks, "\r\n", $st);
}
function ToObject($array) {
    $object = new stdClass();
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $value = ToObject($value);
        }
        if (isset($value)) {
            $object->$key = $value;
        }
    }
    return $object;
}
function ToArray($obj) {
    if (is_object($obj))
        $obj = (array) $obj;
    if (is_array($obj)) {
        $new = array();
        foreach ($obj as $key => $val) {
            $new[$key] = ToArray($val);
        }
    } else {
        $new = $obj;
    }
    return $new;
}

function PT_Secure($string, $censored_words = 1, $br = true) {
    global $mysqli;
    $string = trim($string);
    $string = mysqli_real_escape_string($mysqli, $string);
    $string = htmlspecialchars($string, ENT_QUOTES);
    if ($br == true) {
        $string = str_replace('\r\n', " <br>", $string);
        $string = str_replace('\n\r', " <br>", $string);
        $string = str_replace('\r', " <br>", $string);
        $string = str_replace('\n', " <br>", $string);
    } else {
        $string = str_replace('\r\n', "", $string);
        $string = str_replace('\n\r', "", $string);
        $string = str_replace('\r', "", $string);
        $string = str_replace('\n', "", $string);
    }
    $string = stripslashes($string);
    $string = str_replace('&amp;#', '&#', $string);
    $string = preg_replace("/{{(.*?)}}/", '', $string);
    if ($censored_words == 1) {
        global $config;
        $censored_words = @explode(",", $config['censored_words']);
        foreach ($censored_words as $censored_word) {
            $censored_word = trim($censored_word);
            $string        = str_replace($censored_word, '****', $string);
        }
    }
    return $string;
}
function PT_IsLogged() {
    if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
        $id = PT_GetUserFromSessionID($_SESSION['user_id']);
        if (is_numeric($id) && !empty($id)) {
            return true;
        }
    }

    else if (!empty($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) {
        $id = PT_GetUserFromSessionID($_COOKIE['user_id']);
        if (is_numeric($id) && !empty($id)) {
            return true;
        }
    }

    else {
        return false;
    }
}
function PT_GetUserFromSessionID($session_id, $platform = 'web') {
    global $db;
    if (empty($session_id)) {
        return false;
    }
    $platform   = PT_Secure($platform);
    $session_id = PT_Secure($session_id);
    $return     = $db->where('session_id', $session_id);
    $return     = $db->where('platform', $platform);
    return $db->getValue(T_SESSIONS, 'user_id');
}


function PT_GenerateKey($minlength = 20, $maxlength = 20, $uselower = true, $useupper = true, $usenumbers = true, $usespecial = false) {
    $charset = '';
    if ($uselower) {
        $charset .= "abcdefghijklmnopqrstuvwxyz";
    }
    if ($useupper) {
        $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    }
    if ($usenumbers) {
        $charset .= "123456789";
    }
    if ($usespecial) {
        $charset .= "~@#$%^*()_+-={}|][";
    }
    if ($minlength > $maxlength) {
        $length = mt_rand($maxlength, $minlength);
    } else {
        $length = mt_rand($minlength, $maxlength);
    }
    $key = '';
    for ($i = 0; $i < $length; $i++) {
        $key .= $charset[(mt_rand(0, strlen($charset) - 1))];
    }
    return $key;
}
function PT_Resize_Crop_Image($max_width, $max_height, $source_file, $dst_dir, $quality = 80) {
    $imgsize = @getimagesize($source_file);
    $width   = $imgsize[0];
    $height  = $imgsize[1];
    $mime    = $imgsize['mime'];
    switch ($mime) {
        case 'image/gif':
            $image_create = "imagecreatefromgif";
            $image        = "imagegif";
            break;
        case 'image/png':
            $image_create = "imagecreatefrompng";
            $image        = "imagepng";
            break;
        case 'image/jpeg':
            $image_create = "imagecreatefromjpeg";
            $image        = "imagejpeg";
            break;
        default:
            return false;
            break;
    }
    $dst_img    = @imagecreatetruecolor($max_width, $max_height);
    $src_img    = $image_create($source_file);
    $width_new  = $height * $max_width / $max_height;
    $height_new = $width * $max_height / $max_width;
    if ($width_new > $width) {
        $h_point = (($height - $height_new) / 2);
        @imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
    } else {
        $w_point = (($width - $width_new) / 2);
        @imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
    }
    @imagejpeg($dst_img, $dst_dir, $quality);
    if ($dst_img)
        @imagedestroy($dst_img);
    if ($src_img)
        @imagedestroy($src_img);
}

function PT_CompressImage($source_url, $destination_url, $quality) {
    $info = getimagesize($source_url);
    if ($info['mime'] == 'image/jpeg') {
        $image = @imagecreatefromjpeg($source_url);
        @imagejpeg($image, $destination_url, $quality);
    } elseif ($info['mime'] == 'image/gif') {
        $image = @imagecreatefromgif($source_url);
        @imagegif($image, $destination_url, $quality);
    } elseif ($info['mime'] == 'image/png') {
        $image = @imagecreatefrompng($source_url);
        @imagepng($image, $destination_url);
    }
}
function PT_Time_Elapsed_String($ptime) {
    global $pt, $lang;
    $etime = time() - $ptime;
    if ($etime < 1) {
        return $lang->time_begin;
    }
    $a        = array(
        365 * 24 * 60 * 60 => $lang->year,
        30 * 24 * 60 * 60 => $lang->month,
        24 * 60 * 60 => $lang->day,
        60 * 60 => $lang->hour,
        60 => $lang->minute,
        1 => $lang->second
    );
    $a_plural = array(
        $lang->year => $lang->years,
        $lang->month => $lang->months,
        $lang->day => $lang->days,
        $lang->hour => $lang->hours,
        $lang->minute => $lang->minutes,
        $lang->second => $lang->seconds
    );
    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r = round($d);
            if ($pt->language_type == 'rtl') {
                $time_ago = $lang->time_ago . ' ' . $r . ' ' . ($r > 1 ? $a_plural[$str] : $str);
            } else {
                $time_ago = $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ' . $lang->time_ago;
            }
            return $time_ago;
        }
    }
}
function check_($check) {
    $siteurl = urlencode($_SERVER['SERVER_NAME']);
    $file    = file_get_contents('http://www.playtubescript.com/purchase.php?code=' . $check . '&url=' . $siteurl);
    $check   = json_decode($file, true);
    return $check;
}
function check_success($check) {
    $siteurl = urlencode($_SERVER['SERVER_NAME']);
    $file    = file_get_contents('http://www.playtubescript.com/purchase.php?code=' . $check . '&success=true&url=' . $siteurl);
    $check   = json_decode($file, true);
    return $check;
}
function PT_EditMarkup($text, $link = true) {
    if ($link == true) {
        $link_search = '/\[a\](.*?)\[\/a\]/i';
        if (preg_match_all($link_search, $text, $matches)) {
            foreach ($matches[1] as $match) {
                $match_decode     = urldecode($match);
                $match_decode_url = $match_decode;
                $count_url        = mb_strlen($match_decode);
                $match_url        = $match_decode;
                if (!preg_match("/http(|s)\:\/\//", $match_decode)) {
                    $match_url = 'http://' . $match_url;
                }
                $text = str_replace('[a]' . $match . '[/a]', $match_decode_url, $text);
            }
        }
    }
    return $text;
}
function PT_Markup($text, $link = true) {
    if ($link == true) {
        $link_search = '/\[a\](.*?)\[\/a\]/i';
        if (preg_match_all($link_search, $text, $matches)) {
            foreach ($matches[1] as $match) {
                $match_decode     = urldecode($match);
                $match_decode_url = $match_decode;
                $count_url        = mb_strlen($match_decode);
                if ($count_url > 50) {
                    $match_decode_url = mb_substr($match_decode_url, 0, 30) . '....' . mb_substr($match_decode_url, 30, 20);
                }
                $match_url = $match_decode;
                if (!preg_match("/http(|s)\:\/\//", $match_decode)) {
                    $match_url = 'http://' . $match_url;
                }
                $text = str_replace('[a]' . $match . '[/a]', '<a href="' . strip_tags($match_url) . '" target="_blank" class="hash" rel="nofollow">' . $match_decode_url . '</a>', $text);
            }
        }
    }
    return $text;
}
function covtime($youtube_time) {
    $start = new DateTime('@0'); // Unix epoch
    $start->add(new DateInterval($youtube_time));
    return $start->format('H:i:s');
}

function PT_CreateSession() {
    $hash = sha1(rand(1111, 9999));
    if (!empty($_SESSION['hash_id'])) {
        $_SESSION['hash_id'] = $_SESSION['hash_id'];
        return $_SESSION['hash_id'];
    }
    $_SESSION['hash_id'] = $hash;
    return $hash;
}
function PT_ShortText($text = "", $len = 100) {
    if (empty($text) || !is_string($text) || !is_numeric($len) || $len < 1) {
        return "****";
    }
    if (strlen($text) > $len) {
        $text = mb_substr($text, 0, $len, "UTF-8") . "..";
    }
    return $text;
}
function PT_GetIdFromURL($url = false) {
    if (!$url) {
        return false;
    }
    $slug = @end(explode('_', $url));
    $id   = 0;
    $slug = explode('.', $slug);
    $id   = (is_array($slug) && !empty($slug[0]) && is_numeric($slug[0])) ? $slug[0] : 0;
    return $id;
}
function PT_Decode($text = '') {
    return htmlspecialchars_decode($text);
}
function PT_Backup($sql_db_host, $sql_db_user, $sql_db_pass, $sql_db_name, $tables = false, $backup_name = false) {
    $mysqli = new mysqli($sql_db_host, $sql_db_user, $sql_db_pass, $sql_db_name);
    $mysqli->select_db($sql_db_name);
    $mysqli->query("SET NAMES 'utf8'");
    $queryTables = $mysqli->query('SHOW TABLES');
    while ($row = $queryTables->fetch_row()) {
        $target_tables[] = $row[0];
    }
    if ($tables !== false) {
        $target_tables = array_intersect($target_tables, $tables);
    }
    $content = "-- phpMyAdmin SQL Dump
-- http://www.phpmyadmin.net
--
-- Host Connection Info: " . $mysqli->host_info . "
-- Generation Time: " . date('F d, Y \a\t H:i A ( e )') . "
-- Server version: " . mysqli_get_server_info($mysqli) . "
-- PHP Version: " . PHP_VERSION . "
--\n
SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";
SET time_zone = \"+00:00\";\n
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;\n\n";
    foreach ($target_tables as $table) {
        $result        = $mysqli->query('SELECT * FROM ' . $table);
        $fields_amount = $result->field_count;
        $rows_num      = $mysqli->affected_rows;
        $res           = $mysqli->query('SHOW CREATE TABLE ' . $table);
        $TableMLine    = $res->fetch_row();
        $content       = (!isset($content) ? '' : $content) . "
-- ---------------------------------------------------------
--
-- Table structure for table : `{$table}`
--
-- ---------------------------------------------------------
\n" . $TableMLine[1] . ";\n";
        for ($i = 0, $st_counter = 0; $i < $fields_amount; $i++, $st_counter = 0) {
            while ($row = $result->fetch_row()) {
                if ($st_counter % 100 == 0 || $st_counter == 0) {
                    $content .= "\n--
-- Dumping data for table `{$table}`
--\n\nINSERT INTO " . $table . " VALUES";
                }
                $content .= "\n(";
                for ($j = 0; $j < $fields_amount; $j++) {
                    $row[$j] = str_replace("\n", "\\n", addslashes($row[$j]));
                    if (isset($row[$j])) {
                        $content .= '"' . $row[$j] . '"';
                    } else {
                        $content .= '""';
                    }
                    if ($j < ($fields_amount - 1)) {
                        $content .= ',';
                    }
                }
                $content .= ")";
                if ((($st_counter + 1) % 100 == 0 && $st_counter != 0) || $st_counter + 1 == $rows_num) {
                    $content .= ";\n";
                } else {
                    $content .= ",";
                }
                $st_counter = $st_counter + 1;
            }
        }
        $content .= "";
    }
    $content .= "
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;";
    if (!file_exists('script_backups/' . date('d-m-Y'))) {
        @mkdir('script_backups/' . date('d-m-Y'), 0777, true);
    }
    if (!file_exists('script_backups/' . date('d-m-Y') . '/' . time())) {
        mkdir('script_backups/' . date('d-m-Y') . '/' . time(), 0777, true);
    }
    if (!file_exists("script_backups/" . date('d-m-Y') . '/' . time() . "/index.html")) {
        $f = @fopen("script_backups/" . date('d-m-Y') . '/' . time() . "/index.html", "a+");
        @fwrite($f, "");
        @fclose($f);
    }
    if (!file_exists('script_backups/.htaccess')) {
        $f = @fopen("script_backups/.htaccess", "a+");
        @fwrite($f, "deny from all\nOptions -Indexes");
        @fclose($f);
    }
    if (!file_exists("script_backups/" . date('d-m-Y') . "/index.html")) {
        $f = @fopen("script_backups/" . date('d-m-Y') . "/index.html", "a+");
        @fwrite($f, "");
        @fclose($f);
    }
    if (!file_exists('script_backups/index.html')) {
        $f = @fopen("script_backups/index.html", "a+");
        @fwrite($f, "");
        @fclose($f);
    }
    $folder_name = "script_backups/" . date('d-m-Y') . '/' . time();
    $put         = @file_put_contents($folder_name . '/SQL-Backup-' . time() . '-' . date('d-m-Y') . '.sql', $content);
    if ($put) {
        $rootPath = realpath('./');
        $zip      = new ZipArchive();
        $open     = $zip->open($folder_name . '/Files-Backup-' . time() . '-' . date('d-m-Y') . '.zip', ZipArchive::CREATE | ZipArchive::OVERWRITE);
        if ($open !== true) {
            return false;
        }
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);
        foreach ($files as $name => $file) {
            if (!preg_match('/\bscript_backups\b/', $file)) {
                if (!$file->isDir()) {
                    $filePath     = $file->getRealPath();
                    $relativePath = substr($filePath, strlen($rootPath) + 1);
                    $zip->addFile($filePath, $relativePath);
                }
            }
        }
        $zip->close();
        $table = T_CONFIG;
        $date  = date('d-m-Y');
        $mysqli->query("UPDATE `$table` SET `value` = '$date' WHERE `name` = 'last_backup'");
        $mysqli->close();
        return true;
    } else {
        return false;
    }
}
function pt_size_format($bytes) {
    $size = array('1' => '0MB',
                  '2000000' => '2MB',
                  '6000000' => '6MB',
                  '12000000' => '12MB',
                  '24000000' => '24MB',
                  '48000000' => '48MB',
                  '96000000' => '96MB',
                  '256000000' => '256MB',
                  '512000000' => '512MB',
                  '1000000000' => '1GB',
                  '10000000000' => '10GB');
    return $size[$bytes];
}
// function pt_size_format($bytes) {
//     $kb = 1024;
//     $mb = $kb * 1024;
//     $gb = $mb * 1024;
//     $tb = $gb * 1024;
//     if (($bytes >= 0) && ($bytes < $kb)) {
//         return $bytes . ' B';
//     } elseif (($bytes >= $kb) && ($bytes < $mb)) {
//         return ceil($bytes / $kb) . ' KB';
//     } elseif (($bytes >= $mb) && ($bytes < $gb)) {
//         return ceil($bytes / $mb) . ' MB';
//     } elseif (($bytes >= $gb) && ($bytes < $tb)) {
//         return ceil($bytes / $gb) . ' GB';
//     } elseif ($bytes >= $tb) {
//         return ceil($bytes / $tb) . ' TB';
//     } else {
//         return $bytes . ' B';
//     }
// }
function pt_delete_field($id = false) {
    global $pt, $sqlConnect;
    if (IS_LOGGED == false || !PT_IsAdmin()) {
        return false;
    }
    $id    = PT_Secure($id);
    $table = T_FIELDS;
    $query = mysqli_query($sqlConnect, "DELETE FROM `$table` WHERE `id` = {$id}");
    if ($query) {
        $table  = T_USR_PROF_FIELDS;
        $query2 = mysqli_query($sqlConnect, "ALTER TABLE `$table` DROP `fid_{$id}`;");
        if ($query2) {
            return true;
        }
    }
    return false;
}
function pt_is_url($url = false) {
    if (empty($url)) {
        return false;
    }
    if (filter_var($url, FILTER_VALIDATE_URL)) {
        return true;
    }
    return false;
}
function pt_get_user_ads($placement = 1) {
    global $pt, $db, $StatusAds;
    $country = "";
    if (IS_LOGGED && !empty($pt->user->country_id)) {
        $usr_country = $pt->user->country_id;
        $country = " AND `audience` LIKE '%$usr_country%' ";
    }
    $all_spent = $db->rawQuery('SELECT user_id, SUM(spent) AS spent FROM '.T_USR_ADS.' WHERE status = 1 AND (type = 1 OR type = 2) GROUP BY user_id ORDER BY spent DESC');

    $users_ids = array(0);
    if ($all_spent) {
        foreach ($all_spent as $key => $value) {
            $user_data = $db->where('id',$value->user_id)->getOne(T_USERS);
            if ($user_data && (empty($user_data->total_ads) || $user_data->total_ads == 0 || ($user_data->total_ads > 0 && $user_data->total_ads > $value->spent))) {
                $users_ids[] = $value->user_id;
            }
        }
    }
    $query = "select * FROM ".T_USR_ADS." where status_ad = '".$StatusAds->On."' AND (CAST(day_limit AS DECIMAL(11,2)) > CAST(day_spend AS DECIMAL(11,2)) OR CAST(day_limit AS DECIMAL(11,2)) = 0) AND (lifetime_limit > CAST(spent AS DECIMAL(20,2)) OR lifetime_limit = 0)  AND placement = '".$placement."' AND status = 1 AND user_id IN (".implode(',', $users_ids).") ".$country." ORDER BY RAND() LIMIT 1";
    $ad = $db->rawQuery($query);
    if (!empty($ad) && !empty($ad[0])) {
        $ad = $ad[0];
    }

    return (!empty($ad)) ? $ad : false;
}
function pt_register_ad_views($ad_id = false, $publisher_id = false) {
    global $pt, $db;
    if (empty($ad_id) || empty($publisher_id)) {
        return false;
    }
    $ad     = $db->where('id', $ad_id)->getOne(T_USR_ADS);
    $result = false;
    if (!empty($ad)) {
        $ad_owner     = $db->where('id', $ad->user_id)->getOne(T_USERS);
        $con_price    = $pt->config->ad_v_price;
        $pub_price    = $pt->config->pub_price;
        $ad_trans     = false;
        $is_owner     = false;
        $ad_tans_data = array(
            'results' => ($ad->results += 1)
        );
        if (IS_LOGGED) {
            $is_owner = ($ad->user_id == $pt->user->id) ? true : false;
        }
        if (!array_key_exists($ad->id, $pt->user_ad_cons['uaid_'])) {
            $video_owner = $db->where('id', $publisher_id)->getOne(T_USERS);
            if ((($pt->config->usr_v_mon == 'on' && $pt->config->user_mon_approve == 'off') || ($pt->config->usr_v_mon == 'on' && $pt->config->user_mon_approve == 'on' && $video_owner->monetization == '1')) && $video_owner->video_mon == 1) {

                if (!empty($video_owner) && ($ad->user_id != $video_owner->id)) {
//                    $db->where('id', $publisher_id)->update(T_USERS, array(
//                        'balance' => (($video_owner->balance += $pub_price))
//                    ));
                    $db->insert(T_ADS_TRANS,array('amount' => $pub_price,'type' => 'video', 'ad_id' => $ad->id, 'video_owner' => $publisher_id, 'time' => time()));

                }
            }
            $ad_tans_data['spent']              = ($ad->spent += $con_price);
            $ad_trans                           = true;
            $pt->user_ad_cons['uaid_'][$ad->id] = $ad->id;
            setcookie('_uads', htmlentities(serialize($pt->user_ad_cons)), time() + (10 * 365 * 24 * 60 * 60), '/');
            $db->insert(T_ADS_TRANS,array('amount' => $con_price ,'type' => 'spent', 'ad_id' => $ad->id, 'video_owner' => $publisher_id, 'time' => time()));

        }


        $update = $db->where('id', $ad_id)->update(T_USR_ADS, $ad_tans_data);

        if ($update && $ad_trans && !$is_owner) {
            $ad_value = ($ad_owner->wallet -= $con_price);
            if ($ad_value < 0) {
                $ad_value = 0;
            }
            $db->where('id', $ad_owner->id)->update(T_USERS, array(
                'wallet' => $ad_value
            ));
            if ($ad->day_limit > 0) {
                if ($ad->day == date("Y-m-d")) {
                    $db->where('id',$ad->id)->update(T_USR_ADS,array('day_spend' => ($ad->day_spend + $con_price)));
                }
                else{
                    $db->where('id',$ad->id)->update(T_USR_ADS,array('day_spend' => $con_price ,
                                                                     'day'       => date("Y-m-d")));
                }
            }

            $result = true;
        }
    }
    return $result;
}
function clear_cookies() {
    foreach ($_COOKIE as $key => $value) {
        setcookie($key, $value, time() - 10000, "/");
    }
}
function pt_url_domain($url) {
    $host = @parse_url($url, PHP_URL_HOST);
    if (!$host) {
        $host = $url;
    }
    if (substr($host, 0, 4) == "www.") {
        $host = substr($host, 4);
    }
    if (strlen($host) > 50) {
        $host = substr($host, 0, 47) . '...';
    }
    return $host;
}

function pt_redirect($url) {
    header("Loacation: $url");
    exit();
}

function connect_to_url($url = '', $config = array()) {
    if (empty($url)) {
        return false;
    }
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
    if (!empty($config['POST'])) {
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $config['POST']);
    }
    if (!empty($config['bearer'])) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Bearer ' . $config['bearer']
        ));
    }
    //execute the session
    $curl_response = curl_exec($curl);
    //finish off the session
    curl_close($curl);
    return $curl_response;
}

function connect_to_url_headers($url = '', $config = array()) {
    if (empty($url)) {
        return false;
    }
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_TIMEOUT, 10);
    curl_setopt($curl, CURLOPT_POST, 0);
    curl_setopt($curl, CURLOPT_HEADER, 1);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);

    //execute the session
    $curl_response = curl_exec($curl);
    //finish off the session
    curl_close($curl);

    $headers = [];
    $curl_response = rtrim($curl_response);
    $data = explode("\n",$curl_response);
    $headers['status'] = trim($data[0]);
    array_shift($data);

    foreach($data as $part){

        //some headers will contain ":" character (Location for example), and the part after ":" will be lost, Thanks to @Emanuele
        $middle = explode(":",$part,2);

        //Supress warning message if $middle[1] does not exist, Thanks to @crayons
        if ( !isset($middle[1]) ) { $middle[1] = null; }

        $headers[trim($middle[0])] = trim($middle[1]);
    }

    return $headers;
}


function verify_api_auth($user_id,$session_id, $platform = 'phone') {
    global $db;
    if (empty($session_id) || empty($user_id)) {
        return false;
    }
    $platform   = PT_Secure($platform);
    $session_id = PT_Secure($session_id);
    $user_id    = PT_Secure($user_id);

    $db->where('session_id', $session_id);
    $db->where('user_id', $user_id);
    $db->where('platform', $platform);
    return ($db->getValue(T_SESSIONS, 'COUNT(*)') == 1);
}

function pt_vrequest_exists(){
    global $db,$pt;
    if (!IS_LOGGED) {
        return false;
    }

    $user    = $pt->user->id;
    return ($db->where("user_id",$user)->getValue(T_VERIF_REQUESTS,"count(*)") > 0);
}


function pt_get_announcments() {
    global $pt, $db;
    // if (IS_LOGGED === false) {
    //     return false;
    // }

    $views_table  = T_ANNOUNCEMENT_VIEWS;
    $table        = T_ANNOUNCEMENTS;

    if (IS_LOGGED) {
        $user         = $pt->user->id;
        $subsql       = "SELECT `announcement_id` FROM `$views_table` WHERE `user_id` = '{$user}'";
        $fetched_data = $db->where(" `active` = '1' AND `id` NOT IN ({$subsql}) ")->orderBy('RAND()')->getOne(T_ANNOUNCEMENTS);
    }
    else{
        if (!empty($_COOKIE['ANNOUNCEMENT']) && $_COOKIE['ANNOUNCEMENT'] == 'yes') {
           return false;
        }
        $fetched_data = $db->where(" `active` = '1'")->orderBy('RAND()')->getOne(T_ANNOUNCEMENTS);
    }
    return $fetched_data;
}


function pt_is_banned($ip_address = false){
    global $pt, $db;
    $table = T_BANNED_IPS;
    try {
        $ip    = $db->where('ip_address',$ip_address,'=')->getValue($table,"count(*)");
        return ($ip > 0);
    } catch (Exception $e) {
        return false;
    }
}

function pt_custom_design($a = false,$code = array()){
    global $pt;
    $theme       = $pt->config->theme;
    $data        = array();
    $custom_code = array(
        "themes/$theme/js/header.js",
        "themes/$theme/js/footer.js",
        "themes/$theme/css/custom.style.css",
    );

    if ($a == 'get') {
        foreach ($custom_code as $key => $filepath) {
            if (is_readable($filepath)) {
                $data[$key] = file_get_contents($filepath);
            }
            else{
                $data[$key] = "/* \n Error found while loading: Permission denied in $filepath \n*/";
            }
        }
    }

    else if($a == 'save' && !empty($code)){
        foreach ($code as $key => $content) {
            $filepath = $custom_code[$key];

            if (is_writable($filepath)) {
                @file_put_contents($custom_code[$key],$content);
            }

            else{
                $data[$key] = "Permission denied: $filepath is not writable";
            }
        }
    }

    return $data;
}

function pt_notify($data = array()){
    global $pt, $db;
    if (empty($data) || !is_array($data)) {
        return false;
    }
    if (!empty($data['recipient_id']) && in_array($data['recipient_id'], $pt->blocked_array)) {
        if (empty($data['admin']) && $data['admin'] != 1) {
            return false;
        }
    }

    $t_notif = T_NOTIFICATIONS;
    $query   = $db->insert($t_notif,$data);
    if ($pt->config->push == 1 && empty($data['admin'])) {
        PT_NotificationWebPushNotifier();
    }
    return $query;
}

function pt_get_notification($args = array()){
    global $pt, $db;
    $options  = array(
        "recipient_id" => 0,
        "type" => null,
    );

    $args         = array_merge($options, $args);
    $recipient_id = $args['recipient_id'];
    $type         = $args['type'];
    $data         = array();
    $t_notif      = T_NOTIFICATIONS;

    $db->where('recipient_id',$recipient_id);
    if ($type == 'new') {
        $data = $db->where('seen',0)->getValue($t_notif,'count(*)');
    }

    else{
        $query      = $db->orderBy('id','DESC')->get($t_notif,20);
        foreach ($query as $notif_data_row) {
            $data[] = ToArray($notif_data_row);
        }
    }

    $db->where('recipient_id',$recipient_id);
    $db->where('time',(time() - 432000));
    $db->where('seen',0,'>');
    $db->delete($t_notif);

    return $data;
}

function ffmpeg_duration($filename = false){
    global $pt;

    $ffmpeg_b = $pt->config->ffmpeg_binary_file;
    $output   = shell_exec("$ffmpeg_b -i {$filename} 2>&1");
    $ptrn     = '/Duration: ([0-9]{2}):([0-9]{2}):([^ ,])+/';
    $time     = 30;
    if (preg_match($ptrn, $output, $matches)) {
        $time = str_replace("Duration: ", "", $matches[0]);
        $time_breakdown = explode(":", $time);
        $time = round(($time_breakdown[0]*60*60) + ($time_breakdown[1]*60) + $time_breakdown[2]);
    }

    return $time;
}

function http_respond($data = array()) {
    if (is_callable('fastcgi_finish_request')) {
        session_write_close();
        fastcgi_finish_request();
        return;
    }

    ignore_user_abort(true);
    ob_start();
    $serverProtocol = filter_input(INPUT_SERVER, 'SERVER_PROTOCOL', FILTER_SANITIZE_STRING);
    header($serverProtocol . ' 200 OK');
    header('Content-Encoding: none');
    header('Content-Length: ' . ob_get_length());
    header('Connection: close');
    ob_end_flush();
    ob_flush();
    flush();
}


function get_ip_address() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (filter_var($ip, FILTER_VALIDATE_IP))
                    return $ip;
            }
        } else {
            if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && filter_var($_SERVER['HTTP_X_FORWARDED'], FILTER_VALIDATE_IP))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && filter_var($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'], FILTER_VALIDATE_IP))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && filter_var($_SERVER['HTTP_FORWARDED_FOR'], FILTER_VALIDATE_IP))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && filter_var($_SERVER['HTTP_FORWARDED'], FILTER_VALIDATE_IP))
        return $_SERVER['HTTP_FORWARDED'];
    return $_SERVER['REMOTE_ADDR'];
}

function pt_db_langs() {
    global $pt, $db;
    $data   = array();
    $t_lang = T_LANGS;
    try {
        $query  = $db->rawQuery("DESCRIBE `$t_lang`");
    } catch (Exception $e) {

    }
    foreach ($query as $column) {
        if ($column->Field != "order_priority")
            $data[] = $column->Field;
    }

    unset($data[0]);
    unset($data[1]);
    unset($data[2]);

    return $data;
}

function pt_get_langs($lang = 'english') {
    global $pt, $db;
    $data   = array();
    $t_lang = T_LANGS;
    try {
        $query  = $db->rawQuery("SELECT `lang_key`, `$lang` FROM `$t_lang`");
    } catch (Exception $e) {

    }

    foreach ($query as $item) {
        $data[$item->lang_key] = $item->$lang;
    }

    return $data;
}
function PT_Duration($text) {
    $duration_search = '/\[d\](.*?)\[\/d\]/i';

    if (preg_match_all($duration_search, $text, $matches)) {
        foreach ($matches[1] as $match) {
            $time = explode(":", $match);
            $current_time = ($time[0]*60)+$time[1];
            $text = str_replace('[d]' . $match . '[/d]', '<a  class="hash" href="javascript:void(0)" onclick="go_to_duration('.$current_time.')">' . $match . '</a>', $text);
        }
    }
    return $text;
}
function PT_NotificationWebPushNotifier() {
    global $sqlConnect, $pt,$db;

    if (IS_LOGGED == false) {
        return false;
    }
    if ($pt->config->push == 0) {
        return false;
    }
    $user_id   =  PT_Secure($pt->user->id);
    $to_ids    = array();
    $notifications = $db->where('notifier_id',$user_id)->where('seen',0)->where('sent_push',0)->where('type','admin_notification','<>')->orderBy('id','DESC')->get(T_NOTIFICATIONS);
    if (!empty($notifications)) {
        foreach ($notifications as $key => $notify) {
            $notification_id = $notify->id;
            $to_id           = $notify->recipient_id;
            $to_data         = PT_UserData($notify->recipient_id);
            $ids             = '';
            if (!empty($to_data->device_id)) {
                $ids = array($to_data->device_id);
            }
            $send_array = array(
                'send_to' => $ids,
                'notification' => array(
                    'notification_content' => '',
                    'notification_title' => $pt->user->name,
                    'notification_image' => $pt->user->avatar,
                    'notification_data' => array(
                        'user_id' => $user_id
                    )
                )
            );
            $notify->type_text = '';
            $notificationText = $notify->text;
            if (!empty($notify->type)) {
                $notify->type_text  = $pt->notif_data[$notify->type]['text'];
            }
            $send_array['notification']['notification_content']     = $notify->type_text;
            $send_array['notification']['notification_data']['url'] = $notify->url;
            $send_array['notification']['notification_data']['notify_type'] = $notify->type;
            //$send_array['notification']['notification_data']['user_data'] = PT_UserData($user_id);
            $send_array['notification']['notification_data']['video']  = '';
            if (!empty($notify->video_id)) {
                $video_data = PT_GetVideoByID($notify->video_id,0,1,2);
                $types = array('id','user_id','video_id','thumbnail','video_location','youtube','vimeo','daily','facebook','ok','twitch','twitch_type','age_restriction','privacy','demo','title','is_owner','geo_blocking');
                foreach ($video_data as $key => $value) {
                    if (!in_array($key, $types)) {
                        unset($video_data->{$key});
                    }
                }
                $send_array['notification']['notification_data']['video'] = $video_data;
            }
            //$send_array['notification']['notification_data']['notification_info'] = $notify;
            $send       = PT_SendPushNotification($send_array, 'native');

        }
        $query_get_messages_for_push = $db->where('notifier_id',$user_id)->where('sent_push',0)->update(T_NOTIFICATIONS,array('sent_push' => 1));
    }
    return true;
}

function getBrowser() {
      $u_agent = $_SERVER['HTTP_USER_AGENT'];
      $bname = 'Unknown';
      $platform = 'Unknown';
      $version= "";
      // First get the platform?
      if (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
      } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
      } elseif (preg_match('/iphone|IPhone/i', $u_agent)) {
        $platform = 'IPhone Web';
      } elseif (preg_match('/android|Android/i', $u_agent)) {
        $platform = 'Android Web';
      } else if (preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $u_agent)) {
        $platform = 'Mobile';
      } else if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
      }
      // Next get the name of the useragent yes seperately and for good reason
      if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
      } elseif(preg_match('/Firefox/i',$u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
      } elseif(preg_match('/Chrome/i',$u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
      } elseif(preg_match('/Safari/i',$u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
      } elseif(preg_match('/Opera/i',$u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
      } elseif(preg_match('/Netscape/i',$u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
      }
      // finally get the correct version number
      $known = array('Version', $ub, 'other');
      $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
      if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
      }
      // see how many we have
      $i = count($matches['browser']);
      if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
          $version= $matches['version'][0];
        } else {
          $version= $matches['version'][1];
        }
      } else {
        $version= $matches['version'][0];
      }
      // check if we have a number
      if ($version==null || $version=="") {$version="?";}
      return array(
          'userAgent' => $u_agent,
          'name'      => $bname,
          'version'   => $version,
          'platform'  => $platform,
          'pattern'    => $pattern,
          'ip_address' => get_ip_address()
      );
}
