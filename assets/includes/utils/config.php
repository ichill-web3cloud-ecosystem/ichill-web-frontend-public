<?php
$Common = ToObject(array());
$Common->REGISTER = 0.5;
$Common->WALLET_CONNECTION = 0.5;


$Creator = ToObject(array());
$Creator->UPLOAD_VIDEO = 1;
$Creator->IMPORT_VIDEO = 0.25;
$Creator->VIDEO_VIEWED = 0.5 / 1000;    # 1000 views get 0.5 ISU
$Creator->VIDEO_LIKED = $Creator->VIDEO_VIEWED * 100;  # 1 like equals 100 views
$Creator->VIDEO_DISLIKED = - $Creator->VIDEO_VIEWED * 1000;
$Creator->VIDEO_SHARED = $Creator->VIDEO_LIKED * 10;
$Creator->VIDEO_COMMENTED = $Creator->VIDEO_LIKED;
$Creator->VIDEO_TRENDING = $Creator->VIDEO_LIKED * 100 * 0.75;
$Creator->CHANNEL_SUBSCRIBED = $Creator->VIDEO_LIKED * 10;


$Viewer = ToObject(array());
$Viewer->LIKED = $Creator->VIDEO_LIKED;
$Viewer->COMMENTED = $Creator->VIDEO_LIKED;   # 1 comment equals to 1 liked
$Viewer->COMMENT_LIKED = $Viewer->COMMENTED * 5;   # 1000 votes get 0.5 ISU
$Viewer->VIDEO_TRENDING = $Viewer->LIKED * 100 * 0.25;


$Total = ToObject(array());
$Total->TOTAL_SUBSCRIBED = 1_500_000;
$Total->TOTAL_LIKED = 18_000;
$Total->TOTAL_VIEWED = 9_654_097;
$Total->TOTAL_COMMENDED = 1_062;


$TypeActionVideo = ToObject(array());
$TypeActionVideo->Comment =  'comment';
$TypeActionVideo->Like =  'like';
$TypeActionVideo->Dislike =  'dislike';
$TypeActionVideo->View =  'view';

$Constants = ToObject(array());
$Constants->PRECISION_TO_USD = 4;

$TypeAds = ToObject(array());
$TypeAds->User_ads = 'user_ads';
$TypeAds->Video_ads = 'video_ads';

$TypeActionAds = ToObject(array());
$TypeActionAds->View = 'view';
$TypeActionAds->Click = 'click';

$StatusAds = ToObject(array());
$StatusAds->Waiting = 'waiting';
$StatusAds->On = 'on';
$StatusAds->Off = 'off';
$StatusAds->Rejected = 'rejected';
$StatusAds->Deleted = 'deleted';

$Thumbnail_Default = 'upload/photos/thumbnail.jpg';

?>