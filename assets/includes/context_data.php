<?php
// +------------------------------------------------------------------------+
// | @author Deen Doughouz (DoughouzForest)
// | @author_url 1: http://www.playtubescript.com
// | @author_url 2: http://codecanyon.net/user/doughouzforest
// | @author_email: wowondersocial@gmail.com   
// +------------------------------------------------------------------------+
// | PlayTube - The Ultimate Video Sharing Platform
// | Copyright (c) 2017 PlayTube. All rights reserved.
// +------------------------------------------------------------------------+


$pt->months   = array(
    '1'  => 'January',
    '2'  => 'February',
    '3'  =>'March',
    '4'  =>'April',
    '5'  =>'May',
    '6'  =>'June',
    '7'  =>'July',
    '8'  =>'August',
    '9'  =>'September',
    '10' =>'October',
    '11' =>'November',
    '12' =>'December'
);

$pt->ads_media_types = array(
    'video/mp4',
    'video/mov',
    'video/mpeg',
    'video/flv',
    'video/avi',
    'video/webm',
    'video/quicktime',
    'image/png',
    'image/jpeg',
    'image/gif'
);

$pt->notif_data = array(
	'subscribed_u' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>', 
		'text' => $lang->subscribed_u
	),
	'unsubscribed_u' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-x"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="18" y1="8" x2="23" y2="13"></line><line x1="23" y1="8" x2="18" y2="13"></line></svg>',
		'text' => $lang->unsubscribed_u
	),'subscribed_u_playlist' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-plus"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="20" y1="8" x2="20" y2="14"></line><line x1="23" y1="11" x2="17" y2="11"></line></svg>', 
		'text' => $lang->subscribed_u_playlist
	),
	'unsubscribed_u_playlist' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user-x"><path d="M16 21v-2a4 4 0 0 0-4-4H5a4 4 0 0 0-4 4v2"></path><circle cx="8.5" cy="7" r="4"></circle><line x1="18" y1="8" x2="23" y2="13"></line><line x1="23" y1="8" x2="18" y2="13"></line></svg>',
		'text' => $lang->unsubscribed_u_playlist
	),
	'liked_ur_video' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>',
		'text' => $lang->liked_ur_video
	),
	'disliked_ur_video' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>',
		'text' => $lang->disliked_ur_video
	),
	'liked_ur_activity' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>',
		'text' => $lang->liked_ur_activity
	),
	'disliked_ur_activity' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>',
		'text' => $lang->disliked_ur_activity
	),
	'commented_ur_actvity' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg>',
		'text' => $lang->commented_ur_actvity
	),
	'commented_ur_video' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-message-circle"><path d="M21 11.5a8.38 8.38 0 0 1-.9 3.8 8.5 8.5 0 0 1-7.6 4.7 8.38 8.38 0 0 1-3.8-.9L3 21l1.9-5.7a8.38 8.38 0 0 1-.9-3.8 8.5 8.5 0 0 1 4.7-7.6 8.38 8.38 0 0 1 3.8-.9h.5a8.48 8.48 0 0 1 8 8v.5z"></path></svg>',
		'text' => $lang->commented_ur_video
	),
	'liked_ur_comment' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-up"><path d="M14 9V5a3 3 0 0 0-3-3l-4 9v11h11.28a2 2 0 0 0 2-1.7l1.38-9a2 2 0 0 0-2-2.3zM7 22H4a2 2 0 0 1-2-2v-7a2 2 0 0 1 2-2h3"></path></svg>',
		'text' => $lang->liked_ur_comment
	),
	'disliked_ur_comment' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-thumbs-down"><path d="M10 15v4a3 3 0 0 0 3 3l4-9V2H5.72a2 2 0 0 0-2 1.7l-1.38 9a2 2 0 0 0 2 2.3zm7-13h2.67A2.31 2.31 0 0 1 22 4v7a2.31 2.31 0 0 1-2.33 2H17"></path></svg>',
		'text' => $lang->disliked_ur_comment
	),
	'replied_2ur_comment' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-repeat"><polyline points="17 1 21 5 17 9"></polyline><path d="M3 11V9a4 4 0 0 1 4-4h14"></path><polyline points="7 23 3 19 7 15"></polyline><path d="M21 13v2a4 4 0 0 1-4 4H3"></path></svg>',
		'text' => $lang->replied_2ur_comment
	),
	'added_video' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-video"><polygon points="23 7 16 12 23 17 23 7"></polygon><rect x="1" y="5" width="15" height="14" rx="2" ry="2"></rect></svg>',
		'text' => $lang->added_new_video
	),
	'added_video_playlist' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-video"><polygon points="23 7 16 12 23 17 23 7"></polygon><rect x="1" y="5" width="15" height="14" rx="2" ry="2"></rect></svg>',
		'text' => $lang->added_video_playlist
	),'paid_to_see' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->paid_to_see
	),'donate_to_you' => array(
		'icon' => '<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24" height="24" stroke-width="2"  x="0px" y="0px" class="feather"	 viewBox="0 0 480 480" fill="red" style="enable-background:new 0 0 480 480;" xml:space="preserve"><g>	<g>		<g>			<path d="M466.697,157.744H319.681c12.26-6.352,24.044-14.884,33.608-26.359c17.625-21.135,29.818-44.853,34.336-66.782				c4.97-24.124,0.205-43.661-13.412-55.01c-7.389-6.164-17.07-9.423-27.997-9.423c-27.604,0-61.334,19.895-88.024,51.916				c-7.691,9.223-13.253,19.514-17.243,30.053c-3.914-10.409-9.367-20.576-16.895-29.718C197.426,20.086,163.689,0,136.007,0				c-10.787,0-20.375,3.199-27.726,9.25C94.591,20.518,89.713,40.028,94.547,64.188c4.394,21.959,16.45,45.743,33.946,66.969				c9.516,11.55,21.276,20.16,33.522,26.587H13.302c-5.522,0-10,4.478-10,10v64.764c0,5.523,4.478,10,10,10h33.186V470				c0,5.522,4.478,10,10,10h151.133h64.768h151.13c5.522,0,10-4.478,10-10V242.508h33.178c5.522,0,10-4.477,10-10v-64.764				C476.697,162.222,472.22,157.744,466.697,157.744z M23.302,222.508v-44.764H197.62v44.764H23.302z M197.621,460H66.487V242.508				h131.133V460z M143.927,118.438c-15.439-18.729-26.011-39.389-29.77-58.174c-3.27-16.342-0.779-29.307,6.834-35.572				c3.782-3.113,8.834-4.692,15.016-4.692c21.197,0,50.376,18.138,72.607,45.134c23.729,28.817,22.438,72.473,20.604,90.932				C210.747,154.327,167.65,147.232,143.927,118.438z M262.389,460h-44.768V177.744h44.768V460z M252.38,155.697				c-1.75-18.435-2.827-62.03,21.172-90.803c22.297-26.75,51.497-44.724,72.663-44.724c6.274,0,11.383,1.608,15.189,4.783				c7.577,6.314,9.994,19.295,6.633,35.614c-3.865,18.759-14.558,39.36-30.109,58.011				C314.031,147.255,270.854,154.078,252.38,155.697z M413.52,460H282.389V242.508H413.52V460z M456.698,222.508L456.698,222.508				H282.389v-44.764h174.309V222.508z"/>			<path d="M108.47,428.021c2.63,0,5.21-1.07,7.07-2.93c1.87-1.86,2.93-4.44,2.93-7.07s-1.06-5.21-2.93-7.07				c-1.86-1.86-4.44-2.93-7.07-2.93c-2.63,0-5.21,1.07-7.069,2.93c-1.86,1.86-2.931,4.44-2.931,7.07s1.07,5.21,2.931,7.07				C103.26,426.95,105.84,428.021,108.47,428.021z"/>			<path d="M131.979,381.011c2.64,0,5.21-1.07,7.069-2.931c1.87-1.859,2.931-4.44,2.931-7.069c0-2.63-1.061-5.21-2.931-7.07				c-1.859-1.861-4.429-2.93-7.069-2.93c-2.631,0-5.21,1.069-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07s1.069,5.21,2.93,7.069				C126.77,379.94,129.348,381.011,131.979,381.011z"/>			<path d="M131.979,287.011c2.63,0,5.21-1.07,7.069-2.931c1.87-1.869,2.931-4.44,2.931-7.069c0-2.641-1.061-5.21-2.931-7.08				c-1.859-1.861-4.439-2.931-7.069-2.931c-2.631,0-5.2,1.07-7.07,2.931c-1.86,1.87-2.93,4.439-2.93,7.08				c0,2.63,1.069,5.2,2.93,7.069C126.77,285.94,129.348,287.011,131.979,287.011z"/>			<path d="M155.47,428.021c2.63,0,5.2-1.07,7.061-2.93c1.87-1.86,2.939-4.44,2.939-7.07s-1.069-5.21-2.939-7.07				c-1.861-1.86-4.431-2.93-7.061-2.93c-2.64,0-5.21,1.07-7.08,2.93c-1.859,1.86-2.92,4.44-2.92,7.07c0,2.64,1.061,5.21,2.92,7.07				C150.26,426.95,152.83,428.021,155.47,428.021z"/>			<path d="M108.47,334.021c2.63,0,5.21-1.07,7.07-2.93c1.87-1.87,2.93-4.44,2.93-7.07c0-2.64-1.06-5.21-2.93-7.07				c-1.86-1.871-4.44-2.93-7.07-2.93c-2.63,0-5.21,1.06-7.069,2.93c-1.86,1.86-2.931,4.431-2.931,7.07c0,2.63,1.07,5.21,2.931,7.07				C103.26,332.95,105.84,334.021,108.47,334.021z"/>			<path d="M155.47,334.021c2.63,0,5.2-1.07,7.061-2.93c1.87-1.87,2.939-4.44,2.939-7.07s-1.069-5.21-2.939-7.07				c-1.86-1.87-4.431-2.93-7.061-2.93c-2.64,0-5.22,1.06-7.08,2.93c-1.859,1.86-2.92,4.431-2.92,7.07c0,2.63,1.061,5.21,2.92,7.07				C150.249,332.95,152.83,334.021,155.47,334.021z"/>			<path d="M320.021,428.021c2.63,0,5.21-1.07,7.069-2.93c1.87-1.86,2.931-4.44,2.931-7.07s-1.061-5.21-2.931-7.07				c-1.86-1.86-4.44-2.93-7.069-2.93c-2.631,0-5.211,1.07-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07s1.069,5.21,2.93,7.07				C314.81,426.95,317.39,428.021,320.021,428.021z"/>			<path d="M367,428.021c2.63,0,5.21-1.07,7.07-2.93c1.859-1.86,2.93-4.44,2.93-7.07s-1.07-5.21-2.93-7.07				c-1.86-1.859-4.44-2.93-7.07-2.93s-5.21,1.07-7.07,2.93c-1.859,1.86-2.93,4.44-2.93,7.07s1.07,5.21,2.93,7.07				C361.79,426.95,364.37,428.021,367,428.021z"/>			<path d="M343.521,381.011c2.63,0,5.21-1.07,7.069-2.931c1.86-1.859,2.931-4.44,2.931-7.069c0-2.63-1.07-5.21-2.931-7.07				c-1.859-1.86-4.439-2.93-7.069-2.93c-2.631,0-5.211,1.069-7.07,2.93c-1.86,1.86-2.93,4.44-2.93,7.07s1.069,5.21,2.93,7.069				C338.31,379.94,340.89,381.011,343.521,381.011z"/>			<path d="M343.521,287c2.63,0,5.21-1.06,7.069-2.92c1.86-1.869,2.931-4.44,2.931-7.08c0-2.63-1.07-5.199-2.931-7.06				c-1.859-1.87-4.439-2.94-7.069-2.94c-2.631,0-5.211,1.07-7.07,2.94c-1.86,1.86-2.93,4.43-2.93,7.06				c0,2.641,1.069,5.211,2.93,7.08C338.31,285.94,340.89,287,343.521,287z"/>			<path d="M320.021,334.021c2.63,0,5.21-1.07,7.069-2.93c1.87-1.87,2.931-4.44,2.931-7.07c0-2.64-1.061-5.21-2.931-7.07				c-1.859-1.87-4.439-2.93-7.069-2.93c-2.631,0-5.211,1.06-7.07,2.93c-1.86,1.851-2.93,4.431-2.93,7.07				c0,2.63,1.069,5.21,2.93,7.07C314.81,332.95,317.39,334.021,320.021,334.021z"/>			<path d="M367,334.021c2.63,0,5.21-1.07,7.07-2.93c1.859-1.86,2.93-4.44,2.93-7.07s-1.07-5.21-2.93-7.07				c-1.86-1.87-4.44-2.93-7.07-2.93s-5.21,1.06-7.07,2.93c-1.859,1.86-2.93,4.44-2.93,7.07s1.07,5.21,2.93,7.07				C361.79,332.95,364.37,334.021,367,334.021z"/>		</g>	</g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>',
		'text' => $lang->donate_to_you
	),'rent_video' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->rent_video
	),'rent_to_see' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->rent_to_see
	),'bank_decline' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->bank_decline
	),'bank_pro' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->bank_pro
	),'bank_wallet' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->bank_pro
	),'monetization_accept' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->monetization_accept
	),'monetization_decline' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->monetization_decline
	),'admin_notification' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => ''
	),'started_live_video' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-video"><polygon points="23 7 16 12 23 17 23 7"></polygon><rect x="1" y="5" width="15" height="14" rx="2" ry="2"></rect></svg>',
		'text' => $lang->started_live_video
	),'coinpayments_canceled' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->coinpayments_canceled
	),'coinpayments_approved' => array(
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>',
		'text' => $lang->coinpayments_approved
	)
);
