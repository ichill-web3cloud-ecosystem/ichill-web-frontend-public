<?php

$pt->page        = 'my_funding';

$pt->description = $pt->config->description;
$pt->keyword     = $pt->config->keyword;
$pt->page_url_ = $pt->config->site_url.'/my_funding';
$pt->title = $pt->config->name . ' | ' . $pt->config->title;

$pt->content  = PT_LoadPage('my_funding/content', array());