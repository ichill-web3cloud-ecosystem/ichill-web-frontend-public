
<?php
global $pt;
$keyname = $_POST['keyname'];
$uploadId = $_POST['uploadId'];

try {
    $s3 = PT_InitS3();

    $data = $s3->abortMultipartUpload([
        'Bucket'   => $pt->config->s3_bucket_name,
        'Key'      => $keyname,
        'UploadId' => $uploadId
    ]);
  
}
catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
}