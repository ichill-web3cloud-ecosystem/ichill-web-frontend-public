<?php
global $pt, $db, $Constants, $lang, $Thumbnail_Default;

$limit = ($pt->theme_using == 'youplay') ? 10 : 5;
$top_list = '';
$data          = array();


$db->where('privacy', 0);
$top_data = $db->where('is_movie', 0)->where('user_id',$pt->blocked_array , 'NOT IN')->where('approved',1)->where('is_short', 0)->where('live_time',0)->orderBy('views', 'DESC')->get(T_VIDEOS, $limit);

foreach ($top_data as $key => $video) {
    $video = $pt->video =  PT_GetVideoByID($video, 0, 0, 0);

    $token_video = $video->points;
    $token_to_usd_video = $token_video;
    $token_to_usd_video = round((float)$token_to_usd_video, $Constants->PRECISION_TO_USD);

    $top_list .= PT_LoadPage('home/list', array(
        'ID' => $video->id,
        'TITLE' => $video->title,
        'VIEWS' => number_format($video->views),
        'VIEWS_NUM' => number_format($video->views),
        'USER_DATA' => $video->owner,
        'THUMBNAIL' => $video->medium_thumb,
        'URL' => $video->url,
        'TIME' => $video->time_ago,
        'DURATION' => $video->duration,
        'VIDEO_ID' => $video->video_id_,
        'VIDEO_ID_' => PT_Slug($video->title, $video->video_id),
        'GIF' => $video->gif,
        'TOKEN_VIDEO' => $token_video,
        'TOKEN_TO_USD_VIDEO' => $token_to_usd_video,
    ));
}

$data['status'] = 200;
$data['html']   = $top_list;
$data['no_videos_found_for_now']  = $lang->no_videos_found_for_now;

header('Content-Type: application/json');
echo json_encode($data);
exit();
?>