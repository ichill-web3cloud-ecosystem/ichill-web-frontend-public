<?php
global $pt, $db, $Constants;
$video_id      = (!empty($_GET['video_id']) && is_numeric($_GET['video_id'])) ? $_GET['video_id'] : 0;
$data          = array('status' => 404);
$t_videos      = T_VIDEOS;
$video_next = "";

if (!empty($video_id)) {
    $video_data = $db->where('id', $video_id)->where('user_id', $pt->blocked_array, 'NOT IN')->getOne($t_videos);
    if (!empty($video_data)) {
        $sql_query = "
				SELECT * FROM `$t_videos` 
				WHERE id <> '{$video_id}'  AND user_id NOT IN (" . implode(',', $pt->blocked_array) . ")
				AND privacy = 0
				AND is_movie = 0
				AND is_short = 0
				ORDER BY RAND()
				LIMIT 1 ";

        $videos = $db->rawQuery($sql_query);
        foreach ($videos as $key => $video) {
            $video  = PT_GetVideoByID($video, 0, 0, 0);

            $token_video = $video->points;
            $token_to_usd_video = $token_video;
            $token_to_usd_video = round((float)$token_to_usd_video, $Constants->PRECISION_TO_USD);

            $video_next .= PT_LoadPage('watch/video-sidebar', array(
                'ID' => $video->id,
                'TITLE' => $video->title,
                'URL' => $video->url,
                'THUMBNAIL' => !empty($video->youtube) ? $video->standard_thumb : $video->thumbnail,
                'USER_NAME' => $video->owner->name,
                'VIEWS' => $video->views,
                'TIME' => $video->time_alpha,
                'V_ID' => $video->video_id,
                'GIF' => $video->gif,
                'DURATION' => $video->duration,
                'USER_DATA' => $video->owner,
                'CATEGORY' => $video->category_name,
                'CATEGORY_LINK' => PT_Link('videos/category/'.$video->category_id),
                'TOKEN_VIDEO' => $token_video,
                'TOKEN_TO_USD_VIDEO' => $token_to_usd_video
            ));
        }
        $data['status'] = 200;
        $data['html']   = $video_next;
    }
}

header('Content-Type: application/json');
echo json_encode($data);
exit();
?>