<?php
global $pt, $db;

$data          = array();

$shorts_html = '';
if ($pt->config->shorts_system == 'on') {
    $videos = $db->where('is_short',1)->where('approved',1)->where('time',time() - (60 * 60),'>')->orderBy('views','DESC')->get(T_VIDEOS,6);
    if (!empty($videos) && count($videos) < 5) {
        $ids = array();
        foreach ($videos as $key => $value) {
            $ids[] = $value->id;
        }
        $new_videos = $db->where('is_short',1)->where('approved',1)->where('id',$ids,'NOT IN')->orderBy('id','DESC')->get(T_VIDEOS,6 - count($videos));
        foreach ($new_videos as $key => $value) {
            $videos[] = $value;
        }
    }
    if (empty($videos)) {
        $videos = $db->where('is_short',1)->where('approved',1)->orderBy('id','DESC')->get(T_VIDEOS,6);
    }

    foreach ($videos as $key => $video) {
        $video = PT_GetVideoByID($video, 0, 0, 0);
        $video->url                = PT_Link('shorts/' . $video->video_id);
        $video->load_ajax = $video->video_id;
        if ($pt->config->seo_link == 'on') {
            $video->url                = PT_Link('shorts/' . $video->video_id);
            $video->load_ajax = $video->video_id;
        }

        $pt->video = $video;
        $shorts_html .= PT_LoadPage('home/shorts_list', array(
            'ID' => $video->id,
            'TITLE' => $video->title,
            'VIEWS' => number_format($video->views),
            'VIEWS_NUM' => number_format($video->views),
            'USER_DATA' => $video->owner,
            'THUMBNAIL' => $video->thumbnail,
            'URL' => $video->url,
            'TIME' => $video->time_ago,
            'DURATION' => $video->duration,
            'VIDEO_ID' => $video->video_id_,
            'VIDEO_ID_' => PT_Slug($video->title, $video->video_id),
            'GIF' => $video->gif,
            'LOAD_AJAX' => $video->load_ajax
        ));
    }
}

$data['status'] = 200;
$data['html']   = $shorts_html;

header('Content-Type: application/json');
echo json_encode($data);
exit();
?>