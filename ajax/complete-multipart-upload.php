<?php
global $pt;

$keyname = $_POST['keyname'];
$uploadId = $_POST['uploadId'];
$parts = $_POST['parts'];

$MultipartUpload = array();
foreach ($parts as $value) {
    $PartNumber = $value['PartNumber'];
    $MultipartUpload['Parts'][$PartNumber] = [
        'PartNumber' => $PartNumber,
        'ETag' => $value['ETag'],
    ];
}

try {
    $s3 = PT_InitS3();

    $result = $s3->completeMultipartUpload([
        'Bucket'   => $pt->config->s3_bucket_name,
        'Key'      => $keyname,
        'ACL'    => 'public-read',
        'UploadId' => $uploadId,
        'MultipartUpload'    => $MultipartUpload,
    ]);
    $url = $result['Location'];

    $data = array('status' => 200, 'url' => $url);
}
catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
}
