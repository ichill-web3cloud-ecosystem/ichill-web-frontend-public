<?php
if (IS_LOGGED == false || $pt->config->upload_system != 'on') {
    $data = array('status' => 400, 'error' => 'Not logged in');
    echo json_encode($data);
    exit();
}
if ($pt->user->suspend_upload) {
    $data = array('status' => 400);
    echo json_encode($data);
    exit();
}

if ($pt->config->shorts_system == 'on' && !empty($_POST['video']['is_short']) && $_POST['video']['is_short'] == 'yes') {
    try {
        if (!empty($_POST['video']['duration']) && $_POST['video']['duration'] > $pt->config->shorts_duration) {
            $data = array('status' => 402, 'message' => str_replace('{D}', $pt->config->shorts_duration, $pt->all_lang->max_video_duration));
            echo json_encode($data);
            exit();
        }
    } catch (Exception $e) {
        $data = array('status' => 402, 'message' => $e->getMessage());
        echo json_encode($data);
        exit();
    }
}
if (!PT_IsAdmin()) {
    if ($pt->user->user_upload_limit != '0') {
        if ($pt->user->user_upload_limit != 'unlimited') {
            if (($pt->user->uploads + $_FILES['video']['size']) >= $pt->user->user_upload_limit) {
                $max = pt_size_format($pt->user->user_upload_limit);
                $data = array('status' => 402, 'message' => ($lang->file_is_too_big . ": $max"));
                echo json_encode($data);
                exit();
            }
        }
    } else {
        if ($pt->config->upload_system_type == '0') {
            if ($pt->config->max_upload_all_users != '0' && ($pt->user->uploads + $_FILES['video']['size']) >= $pt->config->max_upload_all_users) {
                $max = pt_size_format($pt->config->max_upload_all_users);
                $data = array('status' => 402, 'message' => ($lang->file_is_too_big . ": $max"));
                echo json_encode($data);
                exit();
            }
        } elseif ($pt->config->upload_system_type == '1') {
            if ($pt->user->is_pro == '0' && ($pt->user->uploads + $_FILES['video']['size']) >= $pt->config->max_upload_free_users && $pt->config->max_upload_free_users != 0) {
                $max = pt_size_format($pt->config->max_upload_free_users);
                $data = array('status' => 402, 'message' => ($lang->file_is_too_big . ": $max"));
                echo json_encode($data);
                exit();
            } elseif ($pt->user->is_pro > '0' && ($pt->user->uploads + $_FILES['video']['size']) >= $pt->config->max_upload_pro_users && $pt->config->max_upload_pro_users != 0) {
                $max = pt_size_format($pt->config->max_upload_pro_users);
                $data = array('status' => 402, 'message' => ($lang->file_is_too_big . ": $max"));
                echo json_encode($data);
                exit();
            }
        }
    }
}

$allowed = 'mp4,mov,webm,mpeg';

$new_string = pathinfo($_POST['video']['name'], PATHINFO_FILENAME) . '.' . strtolower(pathinfo($_POST['video']['name'], PATHINFO_EXTENSION));
$extension_allowed = explode(',', $allowed);
$file_extension = pathinfo($new_string, PATHINFO_EXTENSION);
if (!in_array($file_extension, $extension_allowed)) {
    $data = array('status' => 400, 'error' => $lang->file_not_supported);
    echo json_encode($data);
    exit();
}

$file_info = array(
//        'file' => $_POST['video']['tmp_name'],
    'size' => $_POST['video']['size'],
    'name' => $_POST['video']['name'],
    'type' => $_POST['video']['type'],
    'allowed' => 'mp4,mov,webm,mpeg'
);
$pt->config->s3_upload = 'off';
$pt->config->wasabi_storage = 'off';
$pt->config->ftp_upload = 'off';
$pt->config->spaces = 'off';


$file_upload = PT_ShareFileV1($file_info);
$presigned =  PT_GetPreSignedURLS3Parts($file_upload['filename'], $_POST['video']['chunks']);
$file_upload['pre_signed_url'] = $presigned['urls'];
$file_upload['multipart_upload_id'] = $presigned['multipart_upload_id'];


if (!empty($file_upload['filename'])) {
    $explode3 = @explode('.', $file_upload['name']);
    $file_upload['name'] = $explode3[0];
    $data = array(
        'status' => 200,
        'file_path' => $file_upload['filename'],
        'file_name' => $file_upload['name'],
        'pre_signed_url' => $file_upload['pre_signed_url'],
        'multipart_upload_id' => $file_upload['multipart_upload_id']
    );
    $update = array('uploads' => ($pt->user->uploads += $file_info['size']));
    $db->where('id', $pt->user->id)->update(T_USERS, $update);
    $data['uploaded_id'] = $db->insert(T_UPLOADED, array('user_id' => $pt->user->id,
        'path' => $file_upload['filename'],
        'time' => time()));
    $data['duration'] = $_POST['video']['duration'];

} else if (!empty($file_upload['error'])) {
    $data = array('status' => 400, 'error' => $file_upload['error']);
}
?>