<?php
$data = array('status' => 400);
if (!empty($_POST['search_value'])) {
    $search_value = PT_Secure($_POST['search_value']);

    global $backend_host;
    $url = $backend_host.'/v1/searching/video?text='.url_slug($_POST['search_value']);
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => $url,
        CURLOPT_SSL_VERIFYPEER => true,
        CURLOPT_HTTPHEADER => [ 'Content-Type: application/json', ],

    ));
    curl_setopt($curl, CURLOPT_POST, 0);
    $resp = curl_exec($curl);
    $result = json_decode($resp, true);
    curl_close($curl);

    $search_result = $result['message']['videos'];
    if (!empty($search_result)) {
        $html = '';
        $index = 0;
        foreach ($search_result as $key => $search) {
            $search = (object) $search;
            $search = PT_GetVideoByID($search, 0, 0, 0);
            $html .= "<div class='search-result search-item-$index' ><a href='$search->url'>$search->title</a></div>";
            $index += 1;
        }
        $data = array('status' => 200, 'html' => $html);
    }
}
?>
