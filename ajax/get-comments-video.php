<?php
try {
    global $db, $pt, $Constants, $user;

    $video_id      = (!empty($_GET['video_id']) && is_numeric($_GET['video_id'])) ? $_GET['video_id'] : 0;
    $page      = (!empty($_GET['page']) && is_numeric($_GET['page'])) ? $_GET['page'] : 0;
    $comments_limit     = 20;

    $data          = array('status' => 404);
    $comments = '';

    if (empty($video_id)) {
        $data = array('status' => 400);
        exit();
    }

    $db->where('video_id', $video_id);
    $db->where('pinned', '1','<>')->where('user_id',$pt->blocked_array , 'NOT IN');
    $db->orderBy('id', 'DESC');
    $db->pageLimit = $comments_limit;
    $get_video_comments = $db->paginate(T_COMMENTS, $page);

    $get_video = PT_GetVideoByID($video_id, 1, 1, 2);

    foreach ($get_video_comments as $key => $comment) {
        $comment->text = PT_Duration($comment->text);
        $is_liked_comment = 0;
        $pt->is_comment_owner = false;
        $replies              = "";
        $pt->pin              = false;
        $comment_replies      = $db->where('comment_id', $comment->id)->where('user_id',$pt->blocked_array , 'NOT IN')->get(T_COMM_REPLIES);
        $is_liked_comment     = '';
        $is_comment_disliked  = '';
        $comment_user_data    = PT_UserData($comment->user_id);
        $pt->is_verified      = $comment_user_data->verified == 1;
        foreach ($comment_replies as $reply) {
            $pt->is_reply_owner = false;
            $pt->is_ro_verified = false;
            $reply_user_data    = PT_UserData($reply->user_id);
            $is_liked_reply     = '';
            $is_disliked_reply  = '';
            if (IS_LOGGED) {
                $is_reply_owner = $db->where('id', $reply->id)->where('user_id', $user->id)->getValue(T_COMM_REPLIES, 'count(*)');
                if ($is_reply_owner || $pt->video_owner) {
                    $pt->is_reply_owner = true;
                }

                //Check is this reply  voted by logged-in user
                $db->where('reply_id', $reply->id);
                $db->where('user_id', $user->id);
                $db->where('type', 1);
                $is_liked_reply    = ($db->getValue(T_COMMENTS_LIKES, 'count(*)') > 0) ? 'active' : '';

                $db->where('reply_id', $reply->id);
                $db->where('user_id', $user->id);
                $db->where('type', 2);
                $is_disliked_reply = ($db->getValue(T_COMMENTS_LIKES, 'count(*)') > 0) ? 'active' : '';
            }

            if ($reply_user_data->verified == 1) {
                $pt->is_ro_verified = true;
            }

            //Get related to reply likes
            $db->where('reply_id', $reply->id);
            $db->where('type', 1);
            $reply_likes    = $db->getValue(T_COMMENTS_LIKES, 'count(*)');

            $db->where('reply_id', $reply->id);
            $db->where('type', 2);
            $reply_dislikes = $db->getValue(T_COMMENTS_LIKES, 'count(*)');
            $reply->text = PT_Duration($reply->text);

            $replies    .= PT_LoadPage('watch/replies', array(
                'ID' => $reply->id,
                'TEXT' => PT_Markup($reply->text),
                'TIME' => PT_Time_Elapsed_String($reply->time),
                'USER_DATA' => $reply_user_data,
                'COMM_ID' => $comment->id,
                'LIKES'  => $reply_likes,
                'DIS_LIKES' => $reply_dislikes,
                'LIKED' => $is_liked_reply,
                'DIS_LIKED' => $is_disliked_reply,
                'TOKEN_VIEWER' => 0,
                'TOKEN_TO_USD_VIEWER' => 0
            ));
        }

        if (IS_LOGGED) {
            $is_liked_comment = $db->where('comment_id', $comment->id)->where('user_id', $user->id)->getValue(T_COMMENTS_LIKES, 'count(*)');

            //Check is comment voted by logged-in user
            $db->where('comment_id', $comment->id);
            $db->where('user_id', $user->id);
            $db->where('type', 1);
            $is_liked_comment   = ($db->getValue(T_COMMENTS_LIKES, 'count(*)') > 0) ? 'active' : '';

            $db->where('comment_id', $comment->id);
            $db->where('user_id', $user->id);
            $db->where('type', 2);
            $is_comment_disliked = ($db->getValue(T_COMMENTS_LIKES, 'count(*)') > 0) ? 'active' : '';

            if ($user->id == $comment->user_id || $pt->video_owner) {
                $pt->is_comment_owner = true;
            }
        }
        if (!empty($get_video->stream_name) && $comment->time <= $get_video->live_time) {
            $video_time = GetVideoTime($get_video->time,$comment->time);
            $current_time = '<span class="time pointer" onclick="go_to_duration('.$video_time['current_time'].')"><a href="javascript:void(0)">'.$video_time['time'].'</a> </span>';
        }
        else{
            $current_time = PT_Time_Elapsed_String($comment->time);
        }

        $token = 0;
        $token_to_usd = 0;
        if ($user->id != $comment->user_id && !$pt->video_owner) {
            $token = $comment->points; //PT_TokenCalculationForOneComment($comment->user_id, $get_video->id, $comment->id );
//            $token_to_usd = $token * $usd_conversion_rate;
            $token_to_usd = $token;
            $token_to_usd = round((float)$token_to_usd, $Constants->PRECISION_TO_USD);
        }

        $comments     .= PT_LoadPage('watch/comments', array(
            'ID' => $comment->id,
            'TEXT' => PT_Markup($comment->text),
            'TIME' => $current_time,
            'USER_DATA' => $comment_user_data,
            'LIKES' => $comment->likes,
            'DIS_LIKES' => $comment->dis_likes,
            'LIKED' => $is_liked_comment,
            'DIS_LIKED' => $is_comment_disliked,
            'COMM_REPLIES' => $replies,
            'VID_ID' => $get_video->id,
            'TOKEN_VIEWER' => $token,
            'TOKEN_TO_USD_VIEWER' => $token_to_usd,
            'VIDEO_KEY' => $get_video->video_id
        ));
    }

    $count_comments  = $db->where('video_id', $get_video->id)->where('user_id',$pt->blocked_array , 'NOT IN')->getValue(T_COMMENTS, 'count(*)');

    $data['status'] = 200;
    $data['html']   = $comments;
    $data['count_comments'] = $count_comments;
    $data['isLoadMoreComment'] = $page*$comments_limit < $count_comments;

    header('Content-Type: application/json');
    echo json_encode($data);
    exit();
} catch (Exception $e) {
    echo $e->getMessage();
}
?>