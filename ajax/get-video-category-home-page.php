<?php
global $pt, $db;

$video_categories_html = '';

foreach ($pt->categories_order as $cat_key => $order) {
    $cat_name = ((array)$categories)[strval($cat_key)];
    if (IS_LOGGED && !empty($pt->user->fav_category)) {
        if (in_array($cat_key, $pt->user->fav_category)) {
            $db->where("category_id = '$cat_key'");
            $db->where('privacy', 0);
            $db->orderBy('id', 'DESC');
            $pt->cat_videos = $db->where('is_movie', 0)->where('user_id',$pt->blocked_array , 'NOT IN')->where('approved',1)->where('is_short', 0)->where('live_time',0)->get(T_VIDEOS, 10);
            if (!empty($pt->cat_videos)) {
                $video_categories_html .= PT_LoadPage('home/categories',array(
                    'CATEGORY_ONE_NAME' => $cat_name,
                    'CATEGORY_ONE_ID' => $cat_key
                ));
            }
        }
    }
    else{
        if (!empty($pt->config->fav_category) && is_array($pt->config->fav_category)) {
            if (in_array($cat_key, $pt->config->fav_category)) {
                $db->where("category_id = '$cat_key'");
                $db->where('privacy', 0);
                $db->orderBy('id', 'DESC');
                $pt->cat_videos = $db->where('is_movie', 0)->where('user_id',$pt->blocked_array , 'NOT IN')->where('approved',1)->where('is_short', 0)->where('live_time',0)->get(T_VIDEOS, 10);
                if (!empty($pt->cat_videos)) {
                    $video_categories_html .= PT_LoadPage('home/categories',array(
                        'CATEGORY_ONE_NAME' => $cat_name,
                        'CATEGORY_ONE_ID' => $cat_key
                    ));
                }
            }
        }
        else{
            $db->where("category_id = '$cat_key'");
            $db->where('privacy', 0);
            $db->orderBy('id', 'DESC');
            $pt->cat_videos = $db->where('is_movie', 0)->where('user_id',$pt->blocked_array , 'NOT IN')->where('approved',1)->where('is_short', 0)->where('live_time',0)->get(T_VIDEOS, 10);
            if (!empty($pt->cat_videos)) {
                $video_categories_html .= PT_LoadPage('home/categories',array(
                    'CATEGORY_ONE_NAME' => $cat_name,
                    'CATEGORY_ONE_ID' => $cat_key
                ));
            }
        }
    }
}

$data['status'] = 200;
$data['html']   = $video_categories_html;

header('Content-Type: application/json');
echo json_encode($data);
exit();

?>