<?php
global $db, $pt;

$video_id      = (!empty($_GET['video_id']) && is_numeric($_GET['video_id'])) ? $_GET['video_id'] : 0;
$data          = array('status' => 404);
$t_videos      = T_VIDEOS;
$video_sidebar = "";

$next_page = $_GET['nextPage'] - 1;
$limit_page_size = $_GET['limitPageSize'];
if (!empty($video_id)) {
    $video_data = $db->where('id',$video_id)->where('user_id',$pt->blocked_array , 'NOT IN')->getOne($t_videos);
		if (!empty($video_data)) {
			$video_title    = preg_replace('/[^a-zA-Z0-9_ -]/s',' ',$video_data->title);;
			$sql_query      = "
				SELECT * FROM `$t_videos` 
				WHERE MATCH (title) AGAINST ('$video_title') 
				AND id <> '{$video_id}'  AND user_id NOT IN (".implode(',', $pt->blocked_array).")
				AND privacy = 0
				AND is_movie = 0
				AND is_short = 0
				ORDER BY `id` DESC
				LIMIT $limit_page_size OFFSET {$next_page} ";

            $related_videos = $db->rawQuery($sql_query);
			if (count($related_videos) > 0) {
				foreach ($related_videos as $key => $related_video) {
				    $related_video  = PT_GetVideoByID($related_video, 0, 0, 0);

                    global $Constants;
                    $token_video = $related_video->points;
                    $token_to_usd_video = $token_video;
                    $token_to_usd_video = round((float)$token_to_usd_video, $Constants->PRECISION_TO_USD);

                    $video_sidebar .= PT_LoadPage('watch/video-sidebar', array(
				        'ID' => $related_video->id,
				        'TITLE' => $related_video->title,
				        'URL' => $related_video->url,
				        'THUMBNAIL' => !empty($related_video->youtube) ? $related_video->medium_thumb : $related_video->thumbnail,
				        'USER_NAME' => $related_video->owner->name,
				        'VIEWS' => $related_video->views,
				        'TIME' => $related_video->time_alpha,
				        'V_ID' => $related_video->video_id,
				        'GIF' => $related_video->gif,
				        'DURATION' => $related_video->duration,
                        'USER_DATA' => $related_video->owner,
                        'CATEGORY' => $related_video->category_name,
				        'CATEGORY_LINK' => PT_Link('videos/category/'.$related_video->category_id),
                        'TOKEN_VIDEO' => $token_video,
                        'TOKEN_TO_USD_VIDEO' => $token_to_usd_video
				    ));
				}

				$data['status'] = 200;
				$data['html']   = $video_sidebar;
			}
		}
	}

header('Content-Type: application/json');
echo json_encode($data);
exit();
?>