<?php
  global $api_url;
  if ($_GET['config'] == 'url') {
    $data = array(
        'status' => 200,
        'message' => $api_url,
    );
      
    header('Content-Type: application/json');
    echo json_encode($data);
    exit();
  }
  $data = array('status' => 500, 'error' => 'Internal error');
  echo json_encode($data);
  exit();
?>