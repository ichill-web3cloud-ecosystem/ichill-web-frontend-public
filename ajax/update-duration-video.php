
<?php
global $db;
$id = $_POST['id'];
$duration = $_POST['duration'];

try {
    $video_data = $db->where('id',$id)->getOne(T_VIDEOS);
    if($video_data->duration === "00:00" ){
        $dur = intval($duration);
        $time_format = $dur >= 3600 ? $time_format = "H:i:s" : "i:s";
        $duration = gmdate($time_format, $dur );

        $db->where('id', $id);
        $db->update(T_VIDEOS, array(
            'duration' => $duration
        ));

        echo $duration;
    }
}
catch(Exception $e) {
    echo 'Message: ' .$e->getMessage();
}