<?php
  global $backend_host;
  if (IS_LOGGED == false) {
    $data = array(
        'status' => 400,
        'error' => 'Not logged in'
    );
    http_response_code(400);
    echo json_encode($data);
    exit();
  }

  $jwt_token = get_jwt_token();
  $url = $backend_host.'/v1/transaction/all';
  if (!empty($_GET['page'])) {
    $url = $url.'?page='.$_GET['page'];
  }

  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_FAILONERROR, 1);
  curl_setopt(
    $ch, CURLOPT_HTTPHEADER, [
      'Content-Type: application/json',
      'Authorization: Bearer '.$jwt_token]
  );
  $request = curl_exec($ch);
  $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
  curl_close($ch);

  if (curl_errno($ch)) {
    $error_msg = curl_error($ch);
    echo $error_msg;
    http_response_code($httpcode);
    exit();
  }

  echo $request;
  exit();
?>