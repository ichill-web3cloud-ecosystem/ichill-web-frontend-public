<?php
global $first, $user, $StatusAds;

function replaceSpecialChar($str): array|string
{
    $len = strlen($str);
    for($i = 0; $i < $len; ++$i) // < and not <=, cause the index starts at 0!
    {
        $result = preg_match('/[^a-zA-Z0-9\']/',  $str[$i]);
        if($result){
            $str = substr_replace($str, '\\'.$str[$i], $i, 1);
            $len = strlen($str);
            $i = $i + 2;
        }
    }
    return $str;
}

function updateUrlMediaAds($adId = 0): bool
{
    global $db, $user, $pt;
    $ad_data  = $db->where('id',$adId)->where('user_id',$user->id)->getOne(T_USR_ADS);
    $url_media = "";
    if($pt->config->s3_upload == 'on'){
        $s3 = PT_InitS3();
        $url_media = $s3->getObjectUrl($pt->config->s3_bucket_name, $ad_data->media );
    }
    $update = $db->where('id', $adId)->update(T_USR_ADS, array( "url_media" => $url_media ));
    return $update;
}

if (IS_LOGGED === true) {
    if ($pt->config->user_ads != 'on') {
        header('Location: ' . PT_Link('404'));
        exit;
    }

    if ($first == 'create') {
        $data = array('status' => 400);
        $error = array();
        $type = none;


        if (mb_strlen($_POST['name']) < 5 || mb_strlen($_POST['name']) > 100) {
            $error['invalid_name'] = $lang->invalid_name;
        }
        if ($_POST['cost'] == 1 && !pt_is_url($_POST['url']) ) {
            $error['invalid_url'] = $lang->invalid_url;
        }
        if (mb_strlen($_POST['title']) < 5 || mb_strlen($_POST['title']) > 150) {
            $error['invalid_ad_title'] = $lang->invalid_ad_title;
        }
        if (mb_strlen($_POST['desc']) < 5 ) {
            $error['invalid_description_create_ads'] = $lang->invalid_description_create_ads;
        }
        if(!file_exists($_FILES['media']['tmp_name'])){
            $error['invalid_media'] = $lang->invalid_media_empty;
        }
        if (!file_exists($_FILES['media']['tmp_name']) || !in_array($_FILES['media']['type'], $pt->ads_media_types)) {
            $error['invalid_media'] = $lang->invalid_videoad_media;
        }
        if (file_exists($_FILES['media']['tmp_name']) && $_FILES['media']['size'] > $pt->config->max_upload) {
            $max = pt_size_format($pt->config->max_upload);
            $error_file = ($lang->file_is_too_big . ": $max");
            $error['invalid_media'] = $error_file;
        }
        if(empty($_POST['audience-list'])){
            $error['invalid_audience_list'] = $lang->invalid_audience_list;
        }
        if (!in_array($_POST['placement'], array(1, 2))) {
            $error['placement'] = $lang->error_msg;
        }
        if (!in_array($_POST['cost'], array(1, 2))) {
            $error['cost'] = $lang->error_msg;
        }
        if ($_POST['placement'] == 2) {
            $media_file = getimagesize($_FILES["media"]["tmp_name"]);
            $img_types = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP);
            if (!in_array($media_file[2], $img_types)) {
                $error['invalid_media'] = $lang->invalid_media_with_placement;
            }
        }
        if(empty($_POST['day_limit'])){
            $error['invalid_spent_limit_day'] = $lang->invalid_spent_limit_day_empty;
        }
        if($_POST['day_limit'] > $_POST['lifetime_limit']){
            $error['invalid_spent_limit_day'] = $lang->invalid_spent_limit_day_less_than;
        }
        if(empty($_POST['lifetime_limit'])){
            $error['invalid_total_spent_limit'] = $lang->invalid_total_spent_limit_empty;
        }
        $valid_duration_video = 5;
        if($_POST['duration_video'] < $valid_duration_video){
            $error['invalid_media'] = $lang->invalid_media_greater_than_5;
        }

        if (empty($error)) {
            $file_type = explode("/", $_FILES['media']['type']);

            $insert_data = array(
                "name" => trim(PT_Secure($_POST['name'])),
                "audience" => implode(',', $_POST['audience-list']),
                "category" => $file_type[0],
                "media" => "",
                "url" => trim(urlencode($_POST['url'])),
                "user_id" => $user->id,
                "placement" => intval($_POST['placement']),
                "posted" => time(),
                "headline" => trim(PT_Secure($_POST['title'])),
                "description" => trim(PT_Secure(PT_ShortText($_POST['desc'], 1000))),
                "location" => "",
                "type" => intval($_POST['cost'])
            );

            $file_info = array(
                'file' => $_FILES['media']['tmp_name'],
                'size' => $_FILES['media']['size'],
                'name' => $_FILES['media']['name'],
                'type' => $_FILES['media']['type']
            );

            if (!empty($_POST['day_limit']) && is_numeric($_POST['day_limit']) && $_POST['day_limit'] > 0) {
                $insert_data['day_limit'] = PT_Secure($_POST['day_limit']);
                $insert_data['day'] = date("Y-m-d");
            }

            if (!empty($_POST['lifetime_limit']) && is_numeric($_POST['lifetime_limit']) && $_POST['lifetime_limit'] > 0) {
                $insert_data['lifetime_limit'] = PT_Secure($_POST['lifetime_limit']);
            }

            $file_upload = PT_ShareFile($file_info);
            if (!empty($file_upload)) {
                $insert_data['media'] = $file_upload['filename'];
                $insert = $db->insert(T_USR_ADS, $insert_data);

                updateUrlMediaAds($insert);

                if (!empty($insert)) {
                    $data['status'] = 200;
                    $data['message'] = $lang->ad_published;
                } else {
                    $data['message'] = "Error 500 internal server error!";
                }

            }
        } else {
            $data['message'] = $error;
        }
    }

    if ($first == 'edit') {
        $data = array('status' => 400);
        $error = array();
        $type = none;
        $media = false;
        $cost = false;

        $ad_id = PT_Secure($_POST['id']);
        $ad_data = $db->where('id', $ad_id)->where('user_id', $user->id)->getOne(T_USR_ADS);
        if (empty($ad_data)) {
            $data['status'] = 404;
            $error = true;
        }

        if($ad_data->status_ad == $StatusAds->Waiting || $ad_data->status_ad == $StatusAds->On){
            if ($_POST['cost'] == 1 && !pt_is_url($_POST['url'])  ) {
                $error['invalid_url'] = $lang->invalid_url;
            }
            if ($_POST['title'] != null && mb_strlen($_POST['title']) < 5 || mb_strlen($_POST['title']) > 150) {
                $error['invalid_ad_title'] = $lang->invalid_ad_title;
            }
            if ($_POST['desc'] != null && mb_strlen($_POST['desc']) < 5) {
                $error['invalid_description_create_ads'] = $lang->invalid_description_create_ads;
            }
            if ($_FILES['media'] != null && file_exists($_FILES['media']['tmp_name']) && !in_array($_FILES['media']['type'], $pt->ads_media_types)) {
                $error['invalid_media'] = $lang->invalid_videoad_media;
            }
            if ($_FILES['media'] != null && file_exists($_FILES['media']['tmp_name']) && $_FILES['media']['size'] > $pt->config->max_upload) {
                $max = pt_size_format($pt->config->max_upload);
                $error_file = ($lang->file_is_too_big . ": $max");
                $error['invalid_media'] = $error_file;
            }
            if ($_POST['placement'] == 2 && $_FILES['media'] != null && file_exists($_FILES['media']['tmp_name'])) {
                $media_file = getimagesize($_FILES["media"]["tmp_name"]);
                $img_types = array(IMAGETYPE_GIF, IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_BMP);
                if (!in_array($media_file[2], $img_types)) {
                    $error['invalid_media'] = $lang->invalid_media_with_placement;
                }
            }
            if ($_POST['placement'] != null && !in_array($_POST['placement'], array(1, 2))) {
                $error['placement'] = $lang->error_msg;
            }
            if( intval($ad_data->placement) != $_POST['placement'] && $_FILES['media'] != null && !file_exists($_FILES['media']['tmp_name'])){
                $error['invalid_media'] = $lang->invalid_media_empty;
            }
            $valid_duration_video = 5;
            if($_POST['duration_video'] < $valid_duration_video){
                $error['invalid_media'] = $lang->invalid_media_greater_than_5;
            }
        }
        if($ad_data->status_ad == $StatusAds->Waiting || $ad_data->status_ad == $StatusAds->On || $ad_data->status_ad == $StatusAds->Off){
            if (mb_strlen($_POST['name']) < 5 || mb_strlen($_POST['name']) > 100) {
                $error['invalid_name'] = $lang->invalid_name;
            }
            if (empty($_POST['audience-list'])) {
                $error['invalid_audience_list'] = $lang->invalid_audience_list;
            }
            if (!in_array($_POST['cost'], array(1, 2))) {
                $error['cost'] = $lang->error_msg;
            }
            if (empty($_POST['day_limit'])) {
                $error['invalid_spent_limit_day'] = $lang->invalid_spent_limit_day_empty;
            }
            if ($_POST['day_limit'] > $_POST['lifetime_limit']) {
                $error['invalid_spent_limit_day'] = $lang->invalid_spent_limit_day_less_than;
            }
            if (empty($_POST['lifetime_limit'])) {
                $error['invalid_total_spent_limit'] = $lang->invalid_total_spent_limit_empty;
            }
        }

        if (empty($error)) {
            if($ad_data->status_ad == $StatusAds->Waiting || $ad_data->status_ad == $StatusAds->On || $ad_data->status_ad == $StatusAds->Off){
                $update_data = array(
                    "name" => PT_Secure($_POST['name']),
                    "audience" => implode(',', $_POST['audience-list']),
                    "user_id" => $user->id,
                    "location" => "",
                    "modified_id" => $user->id,
//                    "type" => (($_POST['placement'] == 2) ? 1 : intval($_POST['cost'])),
                    "modified_time" => date('Y-m-d H:i:s', time())
                );
            }
            if($ad_data->status_ad == $StatusAds->Waiting){
                $update_data['type'] = (($_POST['placement'] == 2) ? 1 : intval($_POST['cost']));
            }
            if($ad_data->status_ad == $StatusAds->Waiting || $ad_data->status_ad == $StatusAds->On ){
                $update_data['url'] = urlencode($_POST['url']);
                if($_POST['title'] != null){
                    $update_data['headline'] = PT_Secure($_POST['title']);
                }
                if($_POST['placement'] != null){
                    $update_data['placement'] = intval($_POST['placement']);
                }
                if($_POST['desc'] != null){
                    $update_data['description'] = PT_Secure(PT_ShortText($_POST['desc'], 1000));
                }
            }

            $update_data['day_limit'] = 0;

            if (!empty($_POST['day_limit']) && is_numeric($_POST['day_limit']) && $_POST['day_limit'] > 0) {
                $update_data['day_limit'] = PT_Secure($_POST['day_limit']);
                if (empty($ad_data->day)) {
                    $update_data['day'] = date("Y-m-d");
                }
            } else {
                $update_data['day_limit'] = 0;
                $update_data['day'] = '';
                $update_data['day_spend'] = 0;
            }

            if (!empty($_POST['lifetime_limit']) && is_numeric($_POST['lifetime_limit']) && $_POST['lifetime_limit'] > 0) {
                $update_data['lifetime_limit'] = PT_Secure($_POST['lifetime_limit']);
            }

            if ($_FILES['media'] != null && $_FILES['media']['size'] > 0 && $ad_data->status_ad == $StatusAds->Waiting) {
                $file_info = array(
                    'file' => $_FILES['media']['tmp_name'],
                    'size' => $_FILES['media']['size'],
                    'name' => $_FILES['media']['name'],
                    'type' => $_FILES['media']['type']
                );
                $file_upload = PT_ShareFile($file_info);
                if (!empty($file_upload)) {
                    $file_type = explode("/", $_FILES['media']['type']);
                    $update_data['media'] = $file_upload['filename'];
                    $update_data['category'] = $file_type[0];
                    if($pt->config->s3_upload == 'on'){
                        PT_DeleteFromToS3($ad_data->media);
                    }
                }
            }

            $ad_id = PT_Secure($_POST['id']);
            $update = $db->where('id', $ad_id)->update(T_USR_ADS, $update_data);
            if (!empty($update)) {
                updateUrlMediaAds($ad_id);

                $data['status'] = 200;
                $data['message'] = $error = $lang->ad_saved;;
            } else {
                $data['message'] = "Error 500 internal server error!";
            }
        } else {
            $data['message'] = $error;
        }
    }

    if ($first == 'delete') {
        $request = (!empty($_POST['id']) && is_numeric($_POST['id']));
        if ($request === true) {
            $id = PT_Secure($_POST['id']);
            $ad = $db->where('id', $id)->where('user_id', $user->id)->getOne(T_USR_ADS);
            $s3 = ($pt->config->s3_upload == 'on' || $pt->config->ftp_upload == 'on' || $pt->config->spaces == 'on' || $pt->config->wasabi_storage == 'on') ? true : false;
            if (!empty($ad)) {
                if (file_exists($ad->media)) {
                    unlink($ad->media);
                } else if ($s3 === true) {
                    PT_DeleteFromToS3($ad->media);
                }

                $db->where('id', $id)->where('user_id', $user->id)->delete(T_USR_ADS);
                $data['status'] = 200;
            }
        }
    }

    if ($first == 'toggle-stat') {
        $request = (!empty($_POST['id']) && is_numeric($_POST['id']));
        if ($request === true) {
            $id = PT_Secure($_POST['id']);
            $ad = $db->where('id', $id)->where('user_id', $user->id)->getOne(T_USR_ADS);
            if (!empty($ad)) {
                $stat = ($ad->status == 1) ? 0 : 1;
                $update = array('status' => $stat);
                $db->where('id', $id)->where('user_id', $user->id)->update(T_USR_ADS, $update);
                $data['status'] = 200;
            }
        }
    }

    if($first == 'filter-list'){
        global $db;
        $status = trim($_POST['status']);
        $valueSearch = trim(strtolower($_POST['value_search']));
        $valueSearch = replaceSpecialChar($valueSearch);
        $limit = $_POST['limit'];
        $page = $_POST['page'];
        $offset = ($page - 1) * $_POST['limit'];
        $order_by = $_POST['order_by'];
        $sort = $_POST['sort'];

        $sql = "select * from ".T_USR_ADS." where CASE WHEN '$status' <> 'all' THEN status_ad = '$status' ELSE 1 = 1 END 
        and lower(name) like '%$valueSearch%' 
        and user_id = ".$user->id." ".
        "order by case when '$order_by' = 'CreatedTime' and '$sort' = 'ASC' then  posted end ASC,
             case when '$order_by' = 'CreatedTime' and '$sort' = 'DESC' then  posted end DESC,
             case when '$order_by' = 'Status' and '$sort' = 'ASC' then status_ad end ASC,
             case when '$order_by' = 'Status' and '$sort' = 'DESC' then status_ad end DESC,
             case when '$order_by' = 'Name' and '$sort' = 'ASC' then name end ASC,
             case when '$order_by' = 'Name' and '$sort' = 'DESC' then name end DESC,
             case when '$order_by' = 'Impression' and '$sort' = 'ASC' then impression end ASC,
             case when '$order_by' = 'Impression' and '$sort' = 'DESC' then impression end DESC,
             case when '$order_by' = 'Reach' and '$sort' = 'ASC' then reach end ASC,
             case when '$order_by' = 'Reach' and '$sort' = 'DESC' then reach end DESC,
             case when '$order_by' = 'Clicks' and '$sort' = 'ASC' then clicks end ASC,
             case when '$order_by' = 'Clicks' and '$sort' = 'DESC' then clicks end DESC,
             case when '$order_by' = 'Budget' and '$sort' = 'ASC' then lifetime_limit end ASC,
             case when '$order_by' = 'Budget' and '$sort' = 'DESC' then lifetime_limit end DESC,
             case when '$order_by' = 'Spent' and '$sort' = 'ASC' then spent end ASC,
             case when '$order_by' = 'Spent' and '$sort' = 'DESC' then spent end DESC
        ";
        $sql_paging = $sql." LIMIT ".$limit." OFFSET ".$offset." ; ";
        $users_ad = $db->rawQuery($sql_paging);

        $list_ads = "";
        $currency  = !empty($pt->config->currency_symbol_array[$pt->config->payment_currency]) ? $pt->config->currency_symbol_array[$pt->config->payment_currency] : '$';
        foreach ($users_ad as $ad) {
            $status = getStatusAds($ad->status_ad);
            $pt->status_ad = $ad->status_ad;

            $list_ads   .= PT_LoadPage('ads/list',array(
                'ID' => $ad->id,
                'TYPE' => ($ad->category == 'image') ? 'image' : 'video_library',
                'NAME' => $ad->name,
                'PR_METHOD' => ($ad->type == 1) ? 'Clicks' : 'Views',
                'RESULTS' => $ad->results,
                'SPENT' => number_format($ad->spent,2),
                'ACTIVE' => (($ad->status == 1) ? 'checked' : ''),
                'CURRENCY'   => $currency,
                'ENGAGEMENT' => $ad->engagement,
                'REACH' => $ad->reach,
                'IMPRESSION' => $ad->impression,
                'CLICKS' => $ad->clicks,
                'LIFETIME_LIMIT' =>  number_format($ad->lifetime_limit,2),
                'STATUS' => $status,
            ));
        };
        if(empty($list_ads)){
            $list_ads = '<tr class="odd"><td valign="top" colspan="9" class="dataTables_empty">No data available in table</td></tr>';
        }

        $users_ads_list = $db->rawQuery($sql);
        $total = count($users_ads_list);

        $page_start = 1 + $limit * ($page - 1);
        $page_end = min(($limit * $page), $total);

        $data['status'] = 200;
        $data['list_ads'] = $list_ads;
        $data['page_start'] = $page_start;
        $data['page_end'] = $page_end;
        $data['total'] = $total;
        $data['disable_page_next'] = ($limit * $page) >= $total;
        $data['disable_page_prev'] = $page == 1;
    }
}

if ($first == 'transfer_balance') {
    $data['status'] = 400;
    if (empty($_POST['amount']) || !is_numeric($_POST['amount']) || $_POST['amount'] < 1) {
        $data['message'] = $lang->please_check_details;
    } elseif ($_POST['amount'] > $pt->user->balance) {
        $currency = '$';

        if ($pt->config->payment_currency == 'EUR') {
            $currency = '€';
        }
        $data['message'] = $lang->max_can_transfer . ' ' . $currency . $pt->user->balance;
    } else {
        $amount = PT_Secure($_POST['amount']);
        $user = $db->where('id', $pt->user->id)->getOne(T_USERS);
        $user_balance = $user->balance - $amount;
        $user_wallet = $user->wallet + $amount;
        $db->where('id', $pt->user->id)->update(T_USERS, array('balance' => $user_balance,
            'wallet' => $user_wallet));
        $data['status'] = 200;
        $data['message'] = $lang->balance_transferred;
    }
}