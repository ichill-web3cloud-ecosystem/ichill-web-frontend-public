
<?php
if (!IS_LOGGED) {
    $data = array(
        'status' => 400,
        'error' => 'Not logged in'
    );
    echo json_encode($data);
    exit();
}
global $pt, $user, $db;
$category_id = $_POST['category_id'];

$limit = intval($_POST['limit']);
$page = intval($_POST['page']);
$offset = ($page - 1)*$limit;

if($page == 1){
    $list = '<div class="text-center no-content-found empty_state"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-video-off"><path d="M16 16v1a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V7a2 2 0 0 1 2-2h2m5.66 0H14a2 2 0 0 1 2 2v3.34l1 1L23 7v10"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg>' . $lang->no_videos_found_for_now . '</div>';
}
$final = '';

$query_video = "select * from ".T_VIDEOS." where user_id = {$user->id} and IF({$category_id} <> -1, category_id = {$category_id}, 1 = 1) ";
$videos_list = $db->rawQuery($query_video);

$query = $query_video ." order by id desc limit {$limit} offset {$offset}  ";
$videos = $db->rawQuery($query);

if (!empty($videos)) {
    $len = count($videos);
    foreach ($videos as $key => $video) {
        $video = PT_GetVideoByID($video, 0, 1, 0);
        $pt->last_video = false;
        if ($key == $len - 1) {
            $pt->last_video = true;
        }
        $file_name = 'list';
        $comments_count = $db->rawQuery('SELECT COUNT(*) AS count FROM '.T_COMMENTS.' c WHERE  video_id = '.$video->id);
        if ($video->is_short == 1) {
            $file_name = 'shorts_list';
        }

        global $Constants;
        $token_video = $video->points;
        $token_to_usd_video = $token_video;
        $token_to_usd_video = round((float)$token_to_usd_video, $Constants->PRECISION_TO_USD);

        $final .= PT_LoadPage("video_studio/$file_name", array(
            'ID' => $video->id,
            'USER_DATA' => $video->owner,
            'THUMBNAIL' => !empty($video->youtube) ? $video->medium_thumb : $video->thumbnail,
            'URL'       => $video->url,
            'TITLE'     => $video->title,
            'DESC'      => $video->markup_description,
            'VIEWS'     => number_format($video->views),
            'TIME'      => $video->time_ago,
            'VIDEO_ID_' => PT_Slug($video->title, $video->video_id),
            'V_ID'      => $video->video_id,
            'LIKES'     => number_format($video->likes),
            'DISLIKES'  => number_format($video->dislikes),
            'COMMENTS'  => number_format($comments_count[0]->count),
            'TOKEN_VIDEO' => $token_video,
            'TOKEN_TO_USD_VIDEO' => $token_to_usd_video
        ));
    }
}
if (empty($final)) {
    $final = $list;
}

$totalItem = count($videos_list);
$isLoadMore = !($page * $limit >= $totalItem);

$data['status'] = 200;
$data['html']   = $final;
$data['isLoadMore']  = $isLoadMore;
$data['totalItem'] = $totalItem;
