<?php
header('Content-Type: application/json');


$key = $_POST['key'];
if ($key != 'bQeShVmYq3t6w9z$' ) {
    $data = array(
        'status' => 404,
        'error' => 'Authentication'
    );
    echo json_encode($data);
    exit();
}

$t_videos = T_VIDEOS;
$t_users = T_USERS;

$sql_query  = " select id from `$t_users`  ";
$user_list = $db->rawQuery($sql_query);
foreach($user_list as $key => $user){
    $token_user  = PT_TokenCalculationForUser($user->id);
    $ud_user = array('points' => $token_user );
    $db->where('id', $user->id)->update(T_USERS, $ud_user);
}

$sql_query  = " select id, video_id from `$t_videos`  ";
$video_list = $db->rawQuery($sql_query);
foreach($video_list as $key => $video){
    $token_video = PT_TokenCalculationForVideo($video->video_id);
    $ud_video = array('points' =>  $token_video   );
    $db->where('id', $video->id)->update(T_VIDEOS, $ud_video);
}


$data = array(
    'status' => 200,
);

echo json_encode($data);
exit();

?>