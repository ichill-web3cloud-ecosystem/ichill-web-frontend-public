# Hướng dẫn cài đặt và chạy phần mềm
## 1. Tạo cây thư mục như sau (từ thư mục gốc / )
```
/data
├── config
|   └── <env_name>
|       ├── backend
|       │   ├── config.json <file>
|       │   └── <env_name>.env <file>
|       ├── db
|       │   └── mysql.env <file>
|       ├── meilisearch
|       │   └── config.env <file>
|       └── web
|           ├── config.json <file>
|           └── config.php <file>
├── sources
│   ├── <source_fe>
│   └── <source_be>
└── volumes
    └── <env_name>
          ├── backend
          │   └── logs
          |       ├── combined.log <file>
          |       ├── debug.log <file>
          |       ├── errors.log <file>
          |       └── info.log <file>
          ├── db
          │   └── mysql
          ├── meilisearch
          │   └── data.ms <dir>
          └── web
              └── upload
```
- <env_name> : tên môi trường
  + test
  + development
  + staging
  + production
## 2. Thêm nội dung vào các file config trong thư mục /data/config cho phù hợp
## 3. Vào thư mục `/data/sources/<source_fe>` chạy lệnh:
```
env=<env_name> ./deploy_up.sh -d --build
```
- Tương tự với `/data/sources/<source_be>`
